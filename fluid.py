import XSteam
import math
import subprocess

class Fluid(object):
	''' Template of fluid class, this class is also the None value for fluid '''
	def __init__(self,project,name):
		self.__source = None
		self.__project = project
		self.__name  = name

	@property
	def project(self):
		return self.__project
	@property
	def name(self):
		return self.__name
	@property
	def source(self):
		return self.__source

	#specific volume [m^3/kg]
	def vv_p(self,p):
		return None
	def vl_p(self,p):
		return None
	def vv_T(self,T):
		return None
	def vl_T(self,T):
		return None
	def v_pT(self,p,T):
		return None
	def v_ph(self,p,h):
		return None
	def v_ps(self,p,s):
		return None
	#Temperature [kelvin]
	def Tcrit(self):
		return None
	def Tsat_p(self,p):
		return None
	def T_ph(self,p,h):
		return None
	def T_hs(self,h,s):
		return None
	#Pressure[Pascal]
	def pcrit(self):
		return None
	def psat_T(self,T):
		return None
	def p_hs(self,h,s):
		return None
	#Enthalpy[J/kg]
	def hV_p(self,p):
		return None
	def hL_p(self,p):
		return None
	def hV_T(self,T):
		return None
	def hL_T(self,T):
		return None
	def h_pT(self,p,T):
		return None
	def h_ps(self,p,s):
		return None
	def h_px(self,p,x):
		return None
	def h_Tx(self,T,x):
		return None
	#Entropy[J/(kg*K)]
	def sV_p(self,p):
		return None
	def sL_p(self,p):
		return None
	def sV_T(self,T):
		return None
	def sL_T(self,T):
		return None
	def s_pT(self,p,T):
		return None
	def s_ph(self,p,h):
		return None
	#Quality[-]
	def x_ph(self,p,h):
		return None
	def x_ps(self,p,s):
		return None


class FluidXSteam(Fluid):
	''' Interface with xsteam fluid'''
	def __init__(self,project,name):
		Fluid.__init__(self,project,name)
		self.__source = 'XSteam'

	#specific volume
	def vv_p(self,p):
		if p != None:
			p = p/float(1e5)
			return XSteam.function('vv_p',p,0)
		else:
			return None
	def vl_p(self,p):
		if p != None:
			p = p/float(1e5)
			return XSteam.function('vl_p',p,0)
		else:
			return None
	def vv_T(self,T):
		if T != None:
			T = T-273.15
			return XSteam.function('vv_T',T,0)
		else:
			return None
	def vl_T(self,T):
		if T != None:
			T = T-273.15
			return XSteam.function('vl_T',T,0)
		else:
			return None
	def v_pT(self,p,T):
		if p != None and T != None:
			p = p/float(1e5)
			T = T - 273.15
			return XSteam.function('v_pT',p,T)
		else:
			return None
	def v_ph(self,p,h):
		if p != None and h != None:
			p = p/float(1e5)
			h = h/float(1e3)
			return XSteam.function('v_ph',p,h)
		else:
			return None
	def v_ps(self,p,s):
		if p != None and s != None:
			p = p/float(1e5)
			s = s/float(1e3)
			return XSteam.function('v_ps',p,s)
		else:
			return None

	#Temperature
	def Tcrit(self):
		return 373.995+273.15
	def Tsat_p(self,p):
		if p != None:
			p = p/float(1e5)
			T = XSteam.function('Tsat_p',p,0)
			return T+273.15

		else : 
			return None
	def T_ph(self,p,h):
		if p != None and h != None:
			p = p/float(1e5)
			h = h/float(1e3)
			T = XSteam.function('T_ph',p,h)
			return T+273.15
		else : 
			return None
	def T_hs(self,h,s):
		if h != None and s != None:
			h = h/float(1e3)
			s = s/float(1e3)
			T = XSteam.function('T_hs',h,s)
			return T+273.15
		else : 
			return None
	#Pressure
	def pcrit(self):
		return 22291500
	def psat_T(self,T):
		if T != None :
			T = T -273.15
			p =  XSteam.function('psat_T',T,0)
			return p*float(1e5)
		else : 
			return None
	def p_hs(self,h,s):
		if h != None and s != None:
			h = h/float(1e3)
			s = s/float(1e3)
			p = XSteam.function('p_hs',h,s)
			return p*float(1e5)
		else : 
			return None
	#Enthalpy
	def hV_p(self,p):
		if p != None:
			p = p/1e5
			h = XSteam.function('hV_p',p,0)
			return h*1e3
		else:
			return None
	def hL_p(self,p):
		if p != None:
			p = p/1e5
			h =  XSteam.function('hL_p',p,0)
			return h*1e3
		else :
			return None
	def hV_T(self,T):
		if T != None:
			T = T -273.15
			h = XSteam.function('hV_T',T,0)
			return h*1e3
		else:
			return None
	def hL_T(self,T):
		if T != None:
			T = T -273.15
			h = XSteam.function('hL_T',T,0)
			return h*1e3
		else:
			return None

	def h_pT(self,p,T):
		if p != None and T != None:
			p = p/1e5
			T = T - 273.15
			h = XSteam.function('h_pT',p,T)
			return h*1e3
		else : 
			return None
	def h_ps(self,p,s):
		if p != None and s != None:
			p = p/1e5
			s = s/1e3
			h = XSteam.function('h_ps',p,s)
			return h*1e3
		else : 
			return None
	def h_px(self,p,x):
		if p != None and x != None:
			p = p/1e5
			h = XSteam.function('h_px',p,x)
			return h*1e3
		else : 
			return None
	def h_Tx(self,T,x):
		if T != None and x != None:
			T = T -273.15
			h = XSteam.function('h_Tx',T,x)
			return h*1e3
		else : 
			return None
	
	#Entropy
	def sV_p(self,p):
		if p != None:
			p = p/1e5
			s= XSteam.function('sV_p',p,0)
			return s*1e3
		else:
			return None
	def sL_p(self,p):
		if p != None:
			p = p/1e5
			s = XSteam.function('sL_p',p,0)
			return s*1e3
		else:
			return None
	def sV_T(self,T):
		if T != None:
			T = T-273.15
			s = XSteam.function('sV_T',T,0)
			return s*1e3
		else:
			return None
	def sL_T(self,T):
		if T != None:
			T = T -273.15
			s = XSteam.function('sL_T',T,0)
			return s*1e3
		else:
			return None

	def s_pT(self,p,T):
		if p != None and T != None:
			p = p/1e5
			T = T-273.15
			s =  XSteam.function('s_pT',p,T)
			return s*1e3
		else : 
			return None
	def s_ph(self,p,h):
		if p != None and h != None:
			p = p/1e5
			h = h/1e3
			s = XSteam.function('s_ph',p,h)
			return s*1e3
		else : 
			return None

	#Quality
	def x_ph(self,p,h):
		if p != None and h != None :
			p = p/1e5
			h = h/1e3
			x = XSteam.function('x_ph',p,h)
			return x
		else : 
			return None
	def x_ps(self,p,s):
		if p != None and s != None :
			p = p/1e5
			s = s/1e3
			x =  XSteam.function('x_ps',p,s)
			return x
		else : 
			return None

class RefPropWrapper:
	def __init__(self):
		self.dic={}
	def function(self,f,a,b,fl):
		if fl not in self.dic:
			self.dic[fl]=subprocess.Popen(["python","refpropext.py",fl],stdin=subprocess.PIPE,stdout=subprocess.PIPE)
		self.dic[fl].stdin.write(f+"/"+str(a)+"/"+str(b)+"\n")
		self.dic[fl].stdin.flush()
		r=float(self.dic[fl].stdout.readline())
		return r
refprop=RefPropWrapper()

class FluidREFPROP(Fluid):
	'''Interface with REFPROP fluid'''
	def __init__(self,project,name):
		Fluid.__init__(self,project,name)
		self.__source = 'REFPROP'
		self.__wm = float(refprop.function('wm', 0,0, self.name)/1000.0) # g/mol/1000 => kg/mol
	
	# SI -> REFPROP 
	# p [Pa] = p/float(1e3) [kPa]
	# T [K] = T [K]
	# h [J/kg] = h/self.__wm [J/mol]
	# s [J/(kg-K)] = s/self.__wm [J/(mol-K)]
	# rho [kg/m^3] = rho /1000 /wm   [mol/L]


	#specific volume
	def vv_p(self,p):
		if p != None:
			p = p/float(1e3)
			return refprop.function('vv_p',p,0,self.name)
		else:
			return None
	def vl_p(self,p):
		if p != None:
			p = p/float(1e3)
			return refprop.function('vl_p',p,0,self.name)
		else:
			return None
	def vv_T(self,T):
		if T != None:
			return refprop.function('vv_T',T,0,self.name)
		else:
			return None
	def vl_T(self,T):
		if T != None:
			return refprop.function('vl_T',T,0,self.name)
		else:
			return None

	def v_pT(self,p,T):
		if p != None and T != None:
			p = p/float(1e3)
			v = refprop.function('v_pT',p,T,self.name)
			return v
		else:
			return None
	def v_ph(self,p,h):
		if p != None and h != None:
			p = p/float(1e3)
			h = h*self.__wm
			v =  refprop.function('v_ph',p,h,self.name)
			return v
		else:
			return None
	def v_ps(self,p,s):
		if p != None and s != None:
			p = p/float(1e3)
			s = s*self.__wm
			v = refprop.function('v_ps',p,s,self.name)
			return v
		else:
			return None


	#Temperature K
	def Tsat_p(self,p):
		if p != None:
			p = p/float(1e3)
			T = refprop.function('Tsat_p',p,0, self.name)
			return T
		else : 
			return None

	def T_ph(self,p,h):
		if p != None and h != None:
			p = p/float(1e3)
			h = h*self.__wm
			T = refprop.function('T_ph',p,h, self.name)
			return T
		else : 
			return None

	def T_hs(self,h,s):
		if h != None and s != None:
			h = h*self.__wm
			s = s*self.__wm
			T = refprop.function('T_hs',h,s, self.name)
			return T
		else : 
			return None

	#Pressure
	def psat_T(self,T):
		if T != None :
			p =  refprop.function('psat_T',T,0, self.name)
			return p*1e3
		else : 
			return None

	def p_hs(self,h,s):
		if h != None and s != None:
			h = h*self.__wm 
			s = s*self.__wm 
			p = refprop.function('p_hs',h,s, self.name)
			return p*1e3
		else : 
			return None

	#Enthalpy
	def hV_p(self,p):
		if p != None:
			p = p/float(1e3) 
			h = refprop.function('hV_p',p,0, self.name)
			if h != None:
				return h/self.__wm
			else:
				return None
		else:
			return None
	def hL_p(self,p):
		if p != None:
			p = p/float(1e3)
			h =  refprop.function('hL_p',p,0, self.name)
			if h != None:
				return h/self.__wm
			else:
				return None
		else :
			return None
	def hV_T(self,T):
		if T != None:
			h = refprop.function('hV_T',T,0, self.name)
			if h != None:
				return h/self.__wm
			else:
				return None
		else:
			return None
	def hL_T(self,T):
		if T != None:
			h = refprop.function('hL_T',T,0, self.name)
			if h != None:
				return h/self.__wm
			else:
				return None
		else:
			return None
	def h_pT(self,p,T):
		if p != None and T != None:
			p = p/float(1e3)
			h = refprop.function('h_pT',p,T, self.name)
			return h/self.__wm
		else : 
			return None
	def h_ps(self,p,s):
		if p != None and s != None:
			p = p/float(1e3)
			s = s*self.__wm
			h = refprop.function('h_ps',p,s, self.name)
			return h/self.__wm
		else : 
			return None
	def h_px(self,p,x):
		if p != None and x != None:
			p = p/float(1e3)
			h = refprop.function('h_px',p,x, self.name)
			return h/self.__wm
		else : 
			return None
	def h_Tx(self,T,x):
		if T != None and x != None:
			h = refprop.function('h_Tx',T,x, self.name)
			return h/self.__wm
		else : 
			return None
	
	#Entropy
	def sV_p(self,p):
		if p != None:
			p = p/float(1e3)
			s = refprop.function('sV_p',p,0, self.name)
			if s != None:
				return s/self.__wm
			else:
				return None
		else:
			return None
	def sL_p(self,p):
		if p != None:
			p = p/float(1e3)
			s = refprop.function('sL_p',p,0, self.name)
			if s != None:
				return s/self.__wm
			else:
				return None

		else:
			return None
	def sV_T(self,T):
		if T != None:
			s = refprop.function('sV_T',T,0, self.name)
			if s != None:
				return s/self.__wm
			else:
				return None

		else:
			return None
	def sL_T(self,T):
		if T != None:
			s = refprop.function('sL_T',T,0, self.name)
			if s != None:
				return s/self.__wm
			else:
				return None

		else:
			return None

	def s_pT(self,p,T):
		if p != None and T != None:
			p = p/float(1e3)
			s =  refprop.function('s_pT',p,T, self.name)
			return s/self.__wm
		else : 
			return None
	def s_ph(self,p,h):
		if p != None and h != None:
			p = p/float(1e3)
			h = h*self.__wm
			s = refprop.function('s_ph',p,h, self.name)
			return s/self.__wm
		else : 
			return None

	#Quality
	def x_ph(self,p,h):
		if p != None and h != None :
			p = p/float(1e3)
			h = h*self.__wm
			x = refprop.function('x_ph',p,h, self.name)
			if x == 999.0 :
				return float('NaN')
			else:
				return x
		else : 
			return None

	def x_ps(self,p,s):
		if p != None and s != None :
			p = p/float(1e3)
			s = s*self.__wm
			x =  refprop.function('x_ps',p,s, self.name)
			if x == 999.0 :
				return float('NaN')
			else:
				return x

		else : 
			return None
