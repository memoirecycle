import wx
import GUI3
import notif
import minigui
import element
import os # NEW
import logging
import collections
import fluid

logger = logging.getLogger()

class MyApp(wx.App,object):
	''' Application that contains all object specific to our programm '''
	def OnInit(self):
		self.__name = 'UCycLe'
		self.__project = Project(self)
		self.__hasRefprop = True
		self.__fluidList = {'':fluid.Fluid(self.__project,''),'XSteam Water':fluid.FluidXSteam(self.__project,'XSteam Water')}
		self.getRefpropFluid()
		self.__fluidListName = self.__fluidList.keys()
		self.__fluidListName.sort()

		self.id2type = {}
		self.type2id = {}
		for i in element.allElem:
			self.id2type[i[0]]=i[1]
			self.type2id[i[1]]=i[0]

		self.__gui = GUI3.MyFrame(self)
		self.__gui.Show()
		self.Bind(wx.EVT_IDLE, self.OnIdle)
		return True

	@property
	def name(self):
		return self.__name

	@property
	def fluidList(self):
		return self.__fluidList

	@property
	def fluidListName(self):
		return self.__fluidListName

	@property
	def gui(self):
		return self.__gui
 
	@property
	def project(self):
		return self.__project

	def OnIdle(self,event):
		''' Event that occur when process is not busy'''
		if self.project.todoList != set() :
			a = self.project.todoList.pop()
			a.update()
			self.project.doNotify()

	def getRefpropFluid(self, refpropPath = 'REFPROP/fluids'):
		if self.__hasRefprop == True:
			List = os.listdir(refpropPath)
			List.sort()
			fluidList = {}
			for x in List:
				if x.endswith('.PPF'):continue
				if x.endswith('.BNC'):continue
				fluidList[str(x).replace('.FLD','')]= fluid.FluidREFPROP(self.__project,str(x).replace('.FLD',''))
			self.__fluidList.update(fluidList)

	def loadRefFluid(self,name):
		for x in self.__fluidList:
			if name == x :
				return self.__fluidList[x]

class Project(notif.Notifier):
	'''Project contains all element specific to a cycle'''
	def __init__(self, app):
		notif.Notifier.__init__(self)
		self.__app = app
		self.__elemList = {} # [name : elem]
		self.__nameList = {} # [elem : name]
		self.__currentElem = None
		self.todoList = set() # dans la todoList, il y a des property.wantNotif  = ELEMENT (= des set)
		self.__saveVersion = 'UCycLe saveVersion 1.1'
		self.__saveName = 'Untitled'
		self.__dirName = ''

	@property
	def saveName(self):
		return self.__saveName

	@property
	def dirName(self):
		return self.__dirName
	@property
	def saveVersion(self):
		return self.__saveVersion

	@property
	def app(self):
		return self.__app

	@property
	def elemList(self):
		return self.__elemList

	@property
	def nameList(self):
		return self.__nameList

	@property
	def currentElem(self):
		return self.__currentElem
	
	@currentElem.setter
	def currentElem(self,elem):
		self.__currentElem = elem
	
	@saveName.setter
	def saveName(self, name):
		self.__saveName = str(name)

	@dirName.setter
	def dirName(self, name):
		self.__dirName = str(name)

	def createElem(self, kind, pos):
		# DOC : kind = element.Transfo for example
		assert kind in self.app.type2id
		assert type(pos) == tuple
		elem = kind(self,pos)
		name = elem.generateName()
		self.__elemList[name]=elem
		self.__nameList[elem]=name
		self.doNotify()
		return elem

	def removeElem(self, name):
		if self.__currentElem == self.__elemList[name]:
			self.__currentElem = None
		self.__elemList[name].detachFromAll()
		del self.__nameList[self.__elemList[name]]
		del self.__elemList[name]
		self.doNotify()

	def removeSelectedElem(self):
		#Delete all selected object
		tempList = dict(self.__elemList)
		for z in tempList:
			if tempList[z].selected == True :
				self.removeElem(z)

	def clear(self):
		# empty the project 
		for x in self.__nameList:
			x.detachFromAll()
			x.clear()
		self.__elemList={}
		self.__nameList={}
		self.__currentElem = None
		self.doNotify()

	def isZoneClick(self,point):
		''' method that check if point is inside an element '''
		for z in self.__nameList:
			clickedElem = z.clicked(point)
			if clickedElem != None:
				return clickedElem

	def saveProject(self, fname, dirName): 
		if not fname.endswith('.cyc') :
			fname = fname+'.cyc'
		self.__saveName = fname
		fname = str(fname)
		f = open(os.path.join(dirName,fname),'w') # 'w' = overwrite
		# v: -> version
		f.write('v:'+self.saveVersion +'\n') 

		# s: -> Project name
		f.write('s:'+self.saveName +'\n')

		# n: -> total number of element
		f.write('n:'+str(len(self.elemList))+'\n')

		for x in self.elemList:

			# e: -> name in nameList / genericName / (x,y)
			f.write('e:'+x+'/'+str(self.elemList[x].genericName())+'/'+str(self.elemList[x].pos[0])+'/'+str(self.elemList[x].pos[1])+'\n')
			for p in self.elemList[x].properties:
				if self.elemList[x].properties[p].isImposed :

					# p: -> elem name / prop label / prop value
					if p != 'fluid':
						f.write('p:'+ x+ '/' + \
						str(self.elemList[x].properties[p].label) +'/'+ \
			                        str(self.elemList[x].properties[p].value)+'\n')
					else: #for fluid we save name of the fluid
						f.write('p:'+ x+ '/' + \
						str(self.elemList[x].properties[p].label) +'/'+ \
			                        str(self.elemList[x].properties[p].value.name)+'\n')

		for x in self.elemList:
			for c in self.elemList[x].connection:

				# c: -> name1 in nameList / conn1 name / name2 in nameList / conn2 name if connected
				# c: -> name1 in nameList / conn1 name / None if not connected
				f.write('c:'+str(x)+'/')
				f.write(c +'/')
				if self.elemList[x].connection[c].link == None:
					f.write('None'+'\n')
				else:
					connElem = self.elemList[x].connection[c].link.elem
					a = self.nameList[connElem]
					f.write(str(a)+'/')
					for conn in connElem.connection:
						if connElem.connection[conn].link != None:
							if connElem.connection[conn].link.elem == self.elemList[x]:
								l = conn
					f.write(str(l)+'\n')
		f.close() # DO NOT FORGET
		logger.info('Save project as %s',fname)


	def openProject(self, fname, dirname):
		assert fname.endswith('.cyc')
		logger.info('Start loading project : %s',fname)
		reminderDict = {} # former elemList name [str] : ...
		f=open(dirname+'/'+fname, 'r') # 'r' = read only
		assert f.readline().rstrip() == 'v:'+self.saveVersion.rstrip()
		while 1:
			line=f.readline().rstrip()
			if not line:
				break
			else:
				z =line[0]+line[1]
				data =  line.split(z)[1]
				if z == 'v:':
					assert data == self.saveVersion.rstrip()
				elif z == 'n:':
					self.saveName = data
				elif z == 's':
					pass
				elif z == 'e:':
					(elem, genericName, posX, posY) = data.split('/')
					a = self.createElem(self.app.id2type[genericName], (float(posX), float(posY)))
					reminderDict[elem] = self.nameList[a] 
				elif z == 'p:':
					(elem, label, value) = data.split('/')
					value = data.split(label)[1].lstrip('/')
					if label != 'fluid':
						self.elemList[reminderDict[elem]].properties[label].impose(value)
					else:
						fluidLoaded = self.__app.loadRefFluid(value)
						self.elemList[reminderDict[elem]].properties[label].impose(fluidLoaded)
				elif z == 'c:':
					tup = data.split('/')
					if len(tup) == 3:
						pass
					if len(tup) ==4:
						(elem1, conn1, elem2, conn2)=data.split('/')
						self.elemList[reminderDict[elem1]].connect(self.elemList[reminderDict[elem1]].connection[conn1], self.elemList[reminderDict[elem2]].connection[conn2])
		f.close() # ATTENTION pas oublier
		logger.info('Loading project completed')

	def newProject(self): ###
		self.clear()
		self.saveName = 'Untitled'
		self.doNotify()
		logger.info('New project')

	def numState(self):
		#returns the number of state present in project
		numberState = 0
		for x in self.__nameList:
			if x.genericName() == 'state':
				numberState +=1
		return numberState

	def exportData(self):
		#fname = self.__saveName.rstrip('.cyc')+str('.txt')
		fname = 'data.txt'
		f = open(os.path.join(self.__dirName,fname),'w')
		for x in self.elemList:
			name = self.elemList[x].properties['name'].value
			line = str()
			for xi in range(len(name)+2):
				line += '-'
			f.write(line+'\n')
			f.write('-'+name+'-'+'\n')
			f.write(line+'\n')
			sortedPropList = GUI3.sortBykey(self.elemList[x].properties)
			for p in sortedPropList:
				if self.elemList[x].properties[p].value != None:
					if p == 'fluid':
						f.write(self.elemList[x].properties[p].label+':'+self.elemList[x].properties[p].value.name+'\n')
					elif p == 'name':
						pass
					else :
						value = str('%0.3f'%float(self.elemList[x].properties[p].value))
						f.write(self.elemList[x].properties[p].label+':'+str(value)+'\n')
		f.close()
		logger.info('Print done')



if __name__ == '__main__':
	myApp = MyApp()
	myApp.MainLoop()
