\section{Résolution d'un cycle}\label{resolution}
Avant de commencer à expliquer la façon dont le programme va calculer les cycles thermiques,
il nous a semblé nécessaire de procéder à quelques rappels destinés aux personnes n'en ayant jamais résolus.
Cela nous permettra, de plus, de pointer les éléments qui vont jouer un rôle dans la structure du programme.

  \subsection{Le calcul d'un cycle de Rankine-Hirn}
La résolution d'un cycle de Rankine-Hirn simple nous servira de base de validation pour notre logiciel.
Il nous permettra aussi de donner des exemples d'équations à résoudre.\\

    Le cycle représenté à la figure [\ref{figrankinehirn}] est constitué d'une pompe, d'un
    générateur de vapeur, d'une turbine et d'un condenseur. Entre chaque transformation se trouve un état.
    Le fluide utilisé est l'eau.

	\begin{figure}[!ht]
	\begin{center}
	\includegraphics[width = 7cm]{images/rankinehirn.png}
	\caption{Cycle de Rankine-Hirn simple}
	\label{figrankinehirn}
	\end{center}
	\end{figure}

    L'objectif est de compléter chaque état, c'est-à-dire que nous cherchons à connaître les valeurs de pression, de température, d'enthalpie, d'entropie et d'exergie.
    Nous voulons également connaître le travail produit et les différents rendements du cycle.\\


    Les données usuelles de départ sont la température du fluide de refroidissement au condenseur ( $ T_{froid} = 20\degre C$),
    la température et la pression 
    à la sortie du générateur de vapeur (520\degre C et 40 bars). La turbine possède un rendement isentropique connu (0.88).
    Notre logiciel n'est pas limité à ces informations, l'utilisateur poura fournir d'autres informations pour effectuer le calcul.\\

    À cela, il faut ajouter certaines hypothèses :
    \begin{enumerate}
      \item la pompe est caractérisée par un rendement polytropique connu (0.85);
      \item le générateur de vapeur est isobare;
      \item les changements de phase étant isothermes, nous supposons que le condenseur transforme le fluide en liquide de façon isotherme;
      \item le condenseur fixe la température de sortie du fluide condensé (température du fluide de refroidissement $T_{froid}$ + 10\degre C). \\
    \end{enumerate}

    Il faut en général connaître deux variables d'un état pour trouver 
    les autres, sauf dans le cas d'un fluide diphasique, où il en faut trois, et le cas d'un fluide saturé, où il n'en faut qu'une.
    Ces valeurs se trouvent avec les tables thermodynamiques. \\

    Le titre x est une variable particulière. Il correspond à la proportion de vapeur dans le mélange.
    Il n'existe donc pas, sauf pour les liquides saturés ($x = 0$), les vapeurs saturées ($x = 1$) et
    les mélanges liquide/vapeur.\\

    L'exergie se calcule pour chaque état grâce à la formule suivante :
      \begin{equation*}
	    e = (h-h_0)-T_0 (s-s_0)
      \end{equation*}
    où l'indice 0 correspond à un état référence. \col{Cette référence dépend du type de cycle : ouvert ou fermé.
    Pour les cycles ouverts, les températures et pressions peuvent s'équilibrer par rapport à celles de l'atmosphère.
    Dans ce cas, on pose les références aux valeurs de l'atmosphère, $15\degre C$ et $1$ $bar$. 
    Pour les cycles fermés, un équilibre de pression n'est pas possible, seul
    l'équilibre de température peut avoir lieu. L'état de référence sera pris à la température ambiante 15\degre C et à la pression de saturation de 
    cette température, soit $0.0171$ $bar$. }\\


    \paragraph{État 1} Par hypothèse (3) sur le condenseur, la température à l'état 1 est la même qu'à l'état 4.
\begin{equation}
     T_4 = T_{froid} + 10 = 30 \degre C = 303.15 K
\end{equation}
 
    Le fluide se trouvant en état de liquide saturé, la connaissance de la température suffit donc à compléter l'état grâce aux tables thermodynamiques.\\

    \begin{center}
    \begin{tabular}{c c c c c c c}
      \hline
      température & pression & titre & entropie & enthalpie & exergie \\
      \hline
      \degre C& bar & - & kJ/(kg K) & kJ/kg & kJ/(kg)\\
      \hline
      hypothèse & table & hypothèse & table & table & calculée\\
      30 & 0.04246 & 0 & 0.4367 & 125.75 & 1.58\\
      \hline
    \end{tabular}
    \end{center}

    \paragraph{État 2} Par hypothèse (2) et (4), la pression est la même que celle de l'état 3. L'enthalpie se trouve à l'aide du 
    rendement polytropique de la pompe.
    \col{Par définition, ce rendement vaut : 
    \begin{equation*}
	\eta_{pi} = \frac{\text{travail utile}}{\text{travail moteur}}= \frac{\int{v\Delta p}+g\Delta z+\Delta K}{\Delta h+g\Delta z+\Delta K - Q} 
    \end{equation*}
    En supposant l'adiabaticité et l'absence de variations d'énergie cinétique ($\Delta K = 0 $)
    et potentielle ($g\Delta z = 0$), l'équation du rendement peut se réécrire sous la forme:
    \begin{equation*}
	\eta_{pi} = \frac{\int{v\Delta p}}{\Delta h}
    \end{equation*}
    Si on suppose également que la masse volumique de l'eau $\rho$ est constante :
    \begin{equation*}
	\eta_{pi}= \frac{p_2 - p_1}{\rho (h_2 - h_1)} = 0.85
    \end{equation*}
    Nous obtenons finalement: \begin{equation*}
			      h_2 = h_1 + \frac{(p_2-p_1)}{\rho \eta_{pi}}
			  \end{equation*}
    }
    Nous constatons qu'il nous manque $p_2$ pour résoudre cette équation, or $p_2 = p_3$.
    Il faut donc pouvoir aller chercher des informations dans l'état précédent et dans l'état suivant.
    C'est ici que nous constatons l'importance d'avoir un transfert bidirectionnel des valeurs.
    Les valeurs doivent pouvoir être calculées par plusieurs chemins à la fois.


    Il nous reste à trouver la température à partir des tables. Le titre n'existe pas, car on se situe en
    dehors de la cloche de saturation.\\


    \begin{tabular}{c c c c c c c}
      \hline
      température & pression & titre & entropie & enthalpie & exergie \\
      \hline
      table & hypothèse & n'existe pas & table & calculée & calculée\\
      30 & 40 & - & 0.4393 & 129.74 & 5.58\\
      \hline
    %\label{result1}
    \end{tabular}


    \paragraph{État 3}
    Nous connaissons la pression et la température.
    Le fluide est à l'état vapeur, donc le reste de l'état est connu grâce 
    aux tables thermodynamiques.\\

    \begin{center}
    \begin{tabular}{c c c c c c}
      \hline
      température & pression & titre & entropie & enthalpie & exergie \\
      \hline
      donnée & donnée & n'existe pas & table & table & calculée\\
      520 & 40 & - & 7.1503 & 3491.6 & 1432.93 \\
      \hline
    %\label{result3}
    \end{tabular}
    \end{center}

    \paragraph{État 4} La température est fournie par le fluide de refroidissement augmenté de 10\degre C.
    Pour trouver l'enthalpie, il faut passer par le rendement isentropique de la turbine:
				      \begin{equation*}
					  \eta_{si} = \frac{h_3-h_4}{h_3-h_{4s}}
				      \end{equation*}

   Pour cela, il faut préalablement calculer $h_{4s}$, c'est-à-dire l'enthalpie atteinte de façon isentropique.
    Sachant que $s_{4s} = s_3$ et $p_{4s} = p_4$, nous trouvons avec les tables que $h_{4s} = 2160.9$.

    \begin{center}
    \begin{tabular}{c c c c c c}
      \hline
      température & pression & titre & entropie & enthalpie & exergie \\
      \hline
      donnée & table & table & table & calculée& calculée\\
      30 & 0.04246 & 0.9032 & 7.67 & 2320.6 & 110.18\\
      \hline
    %\label{result4}
    \end{tabular}
    \end{center}

  \paragraph{Analyse énergétique}
L'intérêt d'un cycle réside dans le travail moteur qu'il va fournir et l'énergie qu'il va consommer.
Il est intéressant d'un point de vue thermodynamique de pouvoir réaliser un bilan des puissances du cycle. 
Le calcul des puissances engendrées ou reçues par le fluide est aisé une fois que les enthalpies et le débit sont déterminés. 
La plupart du temps, le calcul de la puissance se résumera simplement à la définition : $$P = \dot{m} \cdot Wm $$
Cette définition devient sous les hypothèses d'adiabaticité et d'absence de variations d'énergie cinétique ou potentielle : $$P = \dot{m} \cdot (h_{out}-h_{in}) $$
Nous prenons la convention que, lorsque le fluide reçoit de l'énergie, la puissance est positive, 
et, lorsqu'il fournit de l'énergie, cette puissance est négative.
% Il faudra cependant bien faire attention à bien comprendre ce que représente cette définition.
%  Par exemple pour la turbine, cette puissance correspond à une
% valeur, qui devra encore être divisée par le rendement mécanique pour obtenir la puissance effective.
%  Pour le condenseur cette puissance sera la puissance que 
% l'on perd dans le fluide de refroidissement.

\subsection{Réflexions pour implémention}
Comme nous avons pu le constater, la résolution d'un cycle nécessite des états, des transformations et des variables.
Les équations sont relativement simples à résoudre.

Du point de vue des variables, nous constatons qu'il faut savoir imposer des valeurs,
 calculer des valeurs et en tirer d'autres des tables thermodynamiques.

Le calcul du cycle en lui-même nécessite de savoir résoudre des équations,
 de pouvoir passer par différents chemins pour aller chercher les valeurs connues.\\

Dans notre étude de faisabilité du logiciel, nous avons parcouru les composants les plus courants
afin d'évaluer les capacités de base que devrait posséder le logiciel.
Dans la plupart des cas, ces composants sont régis par des équations de difficulté équivalente à celles des composants du cycle de Rankine-Hirn.

Il existe cependant certains composants de cycle qui nécessitent des calculs particuliers.
Par exemple les cycles à soutirage nécessitent la résolution d'un système d'équations linéaires pour trouver les débits.
Ces méthodes de calcul ne sont pour le moment pas intégrées au programme. 
Elles pourront faire l'objet d'une amélioration future.

C'est pourquoi nous estimons que pouvoir calculer un cycle de Rankine-Hirn simple convient comme validation pour le logiciel.
