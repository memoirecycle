\section*{Avant-propos}
La création d'un logiciel de calcul de cycles thermiques rassemble les deux matières très différentes que sont la thermodynamique et l'informatique. 
Néanmoins, ce rapport de mémoire s'adressera plutôt aux amateurs de conception de logiciels qu'aux thermodynamiciens.
En effet, même si le logiciel en lui-même vise les gens intéressés par le calcul de cycles thermiques, 
la réflexion informatique et le codage ont constitué la majeure partie de notre travail.
Nous avons créé l'architecture d'un programme qui pourra intégrer plus tard des modules 
nécessitant des notions thermodynamiques plus complexes. 
%Cela explique pourquoi nous ayons principalement mobilisé des connaissances de bases. %comprend pas la phrase.

Nous aspirons à ce que les informaticiens, aussi bien que les mécaniciens, puissent comprendre notre rapport.
C'est pourquoi nous rappellerons des notions d'informatique et de thermodynamique fondamentales.

Ce rapport s'adresse également aux personnes désireuses de continuer l'implémentation du logiciel et des modules supplémentaires.
Nous avons travaillé sur la facilitation de cette reprise.
De la documentation et un code source, disponibles sur Internet, complètent ce rapport.

\newpage
\null
\newpage

\section{Introduction}
Au cours du $20^{e}$ siècle, les développements technologiques se sont de plus en plus orientés vers la recherche de la création
d'une énergie exploitable. Ces recherches ont créé la branche de la physique moderne appelée \textit{thermodynamique}. 
L'une de ses applications concerne les machines thermiques et leurs utilisations, notamment au travers de cycles.

Pour approfondir, enseigner et utiliser ces connaissances acquises, l'outil informatique est devenu incontournable.
C'est l'élément contemporain qui permet de faciliter ces trois actions. 

Notre mémoire a pour but de créer un logiciel de calcul de cycles thermiques moteurs. Notre problématique principale
 repose sur la mise au point d'un logiciel permettant de rendre accessible et interactif le calcul de cycles thermiques.
 Notre seconde problématique réside dans la facilitation de la reprise et l'enrichissement ultérieur de ce logiciel par d'autres personnes.


% La première partie
%de ce rapport contient  les réflexions relatives à ces problématiques, la seconde concerne la méthode et les outils de travail,
% une description de la structure du logiciel et de son utilisation compose la troisième partie tandis que la dernière traite de l'utilisation du logiciel avec un cas concret. 

Notre rapport est divisé en plusieurs parties:
\begin{itemize}
 \item la première contient les réflexions relatives à ces problématiques;
 \item la seconde partie explique la résolution d'un cycle thermique;
 \item la troisième traite de l'utilisation du logiciel avec un cas concret;
 \item la quatrième se compose de la description de la structure du logiciel ainsi que de son utilisation;
 \item la cinquième concerne la méthode et les outils de travail.
\end{itemize}


  \subsection{Cycles thermiques}
  Un cycle thermique est une succession de transformations d'un fluide partant d'un état initial
  et revenant à celui-ci dont le but est de produire du travail. 
   
 L'étymologie du lexème \textit{thermique}, du grec ancien $ \theta \varepsilon \rho \mu o \varsigma $, signifiant \emph{chaud}, nous apprend que ce travail est produit à partir d'une source de chaleur.
  Le côté cyclique présente plusieurs avantages: tout d'abord, comme le système est fini, le travail sera fourni de manière répétitive; enfin, vu que le cycle se compose de plusieurs machines, créer et récupérer ce travail dans une seule boucle est possible.
  Le travail peut alors être utilisé dans d'autres systèmes.

 Le monde qui nous entoure foisonne de cycles thermiques: dans les réfrigérateurs, dans les centrales nucléaires, dans les centrales électriques...
  Ces cycles utilisent de nombreux fluides, dont l'eau est un des plus courants. 
  Pour les cycles frigorifiques, les industries développent des fluides réfrigérants spécifiques à leurs besoins.
  Ces derniers ont parfois des caractéristiques peu communes.
  Dans certaines centrales nucléaires, le caloporteur est le sodium, ou encore le plomb.
  La multiplicité des fluides utilisés égale celle des machines mises en oeuvre pour les transformations:
  pompes, chaudières, moteurs à combustion interne, évaporateurs...  

  \subsection{État de l'art}
  Quelques programmes permettant des calculs de cycles thermiques ont déjà vu le jour. Nous en avons recensés et étudiés quelques-uns. 
  L'ensemble se divise en deux catégories:\\
  \begin{enumerate}
       \item    D'une part, des simulateurs techniques (ChemCad, CoCo) ou plus didactiques (Thermoptim).
		Munis d'une interface graphique interactive, ils permettent de se faire une idée générale du fonctionnement d'un cycle.
		Ils possèdent des bibliothèques de transformations dont les comportements sont déjà modélisés.
		Celles-ci sont implémentées de manière simple. Seuls les paramètres les plus usuels ont été implémentés dans le programme.
	 Ils contiennent dès le début tout ce que nécessite la réalisation d'un pré-projet ou d'un projet simple.\\

       \item	D'autre part, les programmes spécialisés des grands producteurs d'énergie et consorts comme EDF,Electrabel.
		Ils répondent à un besoin industriel et s'adressent aux professionnels.
	    	Souvent réalisés avec un tableur (comme par exemple Excel), 
		ils sont paramétrés de toutes parts pour s'adapter au mieux à une application précise.
		Donnant toutes les caractéristiques des machines utilisées et des circuits, ils sont utilisés en temps réel dans les centrales
		ainsi que dans des projets industriels divers. Ils servent à simuler les comportements,
		faire des prévisions, gérer ces projets. Leur vision du calcul d'un cycle thermique est une vision concrète. Les paramètres demandés sont les 
		paramètres techniques usuels d'utilisation et de maintenance des machines. \\
  \end{enumerate}
  Les programmes de la première catégorie ont,
  dans l'ensemble, les mêmes possibilités.
  Nous pouvons quand même noter une grande différence entre ceux spécifiquement élaborés pour les cycles et les autres.
  Cette différence se marque au niveau du type et du nombre de paramètres à imposer pour pouvoir calculer un cycle. 
  ChemCad, par exemple, a été principalement conçu pour des applications chimiques. Pour réussir à calculer le cycle, il faut systématiquement lui donner
  la pression et la température aux endroits critiques.
  Parmi les lacunes principales de nombreux programmes au niveau thermodynamique se retrouve l'absence de l'exergie.

  Ces programmes ont tous leur particularité et leur fonctionnement propre.
  Une description plus précise ainsi que des captures d'écran des programmes étudiés se trouvent en annexe.
  \newpage

  \subsection{Valeur ajoutée}
  L'investissement nécessaire pour l'acquisition de programmes (prix, implémentation dans le système...) explique pourquoi nous n'en avons testé qu'un nombre réduit.
  De plus, la bonne prise en main des logiciels les plus poussés nécessite de consulter un expert.
  De nombreux autres sont simplement inaccessibles aux particuliers.
  Même pour tester le logiciel, il est nécessaire de fournir des références pour pouvoir acheter une
  version d'essai. Les seuls logiciels gratuits disponibles sont CoCo et Thermoptim. Nous avons également eu l'occasion d'essayer ChemCad.\\

  Notre travail a pour but de fournir une version accessible et didactique d'un tel logiciel. 
  Cette application peut cibler différents publics:
  \begin{itemize}
        \item les étudiants intéressés par la découverte des cycles thermiques au moyen d'un outil informatique;
	\item les chercheurs, qui pourront implémenter aisément des machines prototypes;
	\item le corps enseignant, car le logiciel pourra s'adapter à d'autres applications dans d'autres matières au moyen de nouveaux modules;
	\item l'UCL, qui aura un tel logiciel à sa disposition.\\
  \end{itemize}

  Nous avons placé le code sous licence MIT.
  C'est une licence de logiciel libre et Open Source.
  Elle donne à toute personne recevant le logiciel le droit illimité de l'utiliser,
  le copier, le modifier, le fusionner, le publier, le distribuer, le vendre et de changer sa licence.
  La seule obligation est de mettre le nom des auteurs avec la notice de copyright.

  \subsection{Objet de l'étude} 
  Comme vu ci-dessus, nous nous sommes fixé l'objectif de réaliser un logiciel interactif, modulable et didactique.
  Le logiciel devra notamment contenir des bilans énergétiques sous forme graphique. 
  Cet énoncé très large demande plus de précisions. \\

  Par logiciel, nous entendons un programme avec une interface graphique, qui puisse être utilisé intuitivement et facilement.
  Pour répondre à cette demande, nous avons étudié et analysé différents programmes existants reposant sur un fonctionnement similaire.
  Nous recherchons l'ergonomie: l'objectif est d'être \emph{user friendly}. La plupart de nos choix ont été dictés par
 le bon sens et les critiques que nous ont
  inspirées les programmes testés. Nous en avons tiré différents critères de maniabilité. Des informations détaillées 
  sur ces tests se trouvent dans les annexes.\\
  \newpage

  Du point de vue du cycle thermique, l'objectif est d'avoir des cycles les plus ouverts possibles: ils doivent pouvoir
  calculer les états sur la base des transformations et inversement. Le cycle doit pouvoir être calculé avec un minimum
  d'informations.\\

  L'objectif à long terme est de pouvoir implémenter des cycles exotiques avec les fluides les plus divers.
  %Nous avons élaboré la base de ce projet, c'est-à-dire une structure informatique. 
  Notre but est d'avoir une structure informatique de base réalisant les opérations fondamentales nécessaires où des modules pourront être aisément ajoutés.
  Tout au long du rapport, nous indiquerons les pistes d'amélioration auxquelles nous avons songé.
  Pour les fluides et leurs tables thermodynamiques, il existe une pléthore de bibliothèques numériques.
  Nous n'allons donc pas mettre au point cette bibliothèque, mais permettre un interfaçage avec celles existantes.
  
  En pratique, nous allons implémenter un cycle de Rankine-Hirn avec ses composants. Cela nous permettra de valider notre structure.
