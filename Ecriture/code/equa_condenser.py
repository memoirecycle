	#Condenser Equation
	eq = equa.Equation()

	#Hypothesis : condenser is isobar and impose saturation
	eq.addEquation(V('pressure','I1')-V('pressure','O1'))
	eq.addEquation(V('quality','O1'))
	eq.addEquation(V('temperature','O1')-V('temperature out'))
	eq.addEquation(V('energy lost')+V('enthalpy','I1')-V('enthalpy','O1'))
	eq.addEquation(V('power lost')-V('flow','I1')*V('energy lost'))


