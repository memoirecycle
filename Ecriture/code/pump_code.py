class Pump(TransfoInOut): 
	''' Isentropic pump'''

	genericNumber = 0 
	genericname = 'pump'

	#Equations
	equaPump = equa.Equation()
	equaPump.addEquation(V('pressure','O1')-V('pressure','I1') -V('pressure delta'))
	# eta_pi = v dp / dh
	equaPump.addEquation(V('polytropic efficiency')-V('specific volume','I1')* \
			(V('pressure','O1')-V('pressure','I1'))/ \
			((V('enthalpy','O1')-V('enthalpy','I1')))) 
	equaPump.addEquation(V('work')+V('enthalpy','I1')-V('enthalpy','O1'))
	equaPump.addEquation(V('power lost')-V('flow','I1')*V('work')/V('mechanical efficiency'))
	equaPump.processTodo()

	def __init__(self,project,pos):
		TransfoInOut.__init__(self,project,pos)
			# Add icon
		self._bmp = wx.Image("images/element/"+ self.genericName() +'.png').ConvertToBitmap()

			# Set icon clickable
		self.__zone = (self.pos,(self.pos[0]+75,self.pos[1]+55))

			# Add Connection
		self.conn1 = self.addConn('fromElem',(70,22),'I1')
		self.conn2 = self.addConn('toElem',(-5,22),'O1')

			# Add property
		self.doProp([('pressure delta','floatvar',1),\
			('polytropic efficiency','floatvar',2),\
			('mechanical efficiency','floatvar',3),\
			('work','floatvar',4),\
			('power lost','floatvar',5)])

	@property
	def zone(self):
		'''The zone is the size of the element's picture,
		the area where you can click on panel '''
		self.__zone = (self.pos,(self.pos[0]+75,self.pos[1]+55))
		return self.__zone
	
	def update(self):
		''' Compute transformation's equation'''
		elemUpdate(self,TransfoInOut.equaTransfoInOut.equationList)
		elemUpdate(self,Pump.equaPump.equationList)

allElem.add((Pump.genericname, Pump)) # Pump add herself in the Application
