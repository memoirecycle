import wx
import notif
import prop
from connection import Connection
import collections
import equa
from equa import V
from equa import K
import logging

logger = logging.getLogger()

global allElem
''' Contains State and all the Transformation in this file '''
allElem = set() # set(tuple(genericname, klass))

def isPointInZone(point,zone): 
	''' A zone is where you see Element or Connection on GUI panel '''
	if point[0]  >= zone[0][0] \
	and point[1] >= zone[0][1] \
	and point[0] <= zone[1][0] \
	and point[1] <= zone[1][1] :
		return True
	else :
		return False

def clicked(elem,point):
	''' Return element or connection clicked '''
	connMatch = False
	myzone = elem.zone
	if elem.genericName() != 'state' \
	or (elem.genericName() == 'state' \
	and elem.connection['I1'].link == None \
	and elem.connection['O1'].link == None): 

		for x in elem.connection:
			myconnzone = ((elem.pos[0]+elem.connection[x].dpos[0],elem.pos[1]+elem.connection[x].dpos[1]),\
				(elem.pos[0]+elem.connection[x].dpos[0]+10,elem.pos[1]+elem.connection[x].dpos[1]+10))
	
			if isPointInZone(point,myconnzone):
				connMatch = True
				return elem.connection[x]

	else:
		if elem.genericName() == 'state' and elem.connection['I1'].link == None : # In case where O1 != None
			x = 'I1'
			myconnzone = ((elem.pos[0]+elem.connection[x].dpos[0],elem.pos[1]+elem.connection[x].dpos[1]),\
				(elem.pos[0]+elem.connection[x].dpos[0]+10,elem.pos[1]+elem.connection[x].dpos[1]+10))
			if isPointInZone(point,myconnzone):
				connMatch = True
				return elem.connection[x]
		elif elem.genericName() == 'state' and elem.connection['O1'].link == None : # In case where I1 != None
			x = 'O1'
			myconnzone = ((elem.pos[0]+elem.connection[x].dpos[0],elem.pos[1]+elem.connection[x].dpos[1]),\
					(elem.pos[0]+elem.connection[x].dpos[0]+10,elem.pos[1]+elem.connection[x].dpos[1]+10))
			if isPointInZone(point,myconnzone):
				connMatch = True
				return elem.connection[x]
		# In case where I1 and O1 != None, we just need to do nothing (so we do not check Connection)

	if isPointInZone(point,myzone) and connMatch == False:
			return elem


def elemUpdate(element,equationList):
	''' Check in Element's equation if it can find solution and tell them'''
	for x in equationList :
		result = equa.evalEquation(x,element)
		if result != None:
			result[0].tell(result[1],set(result[2]))

################################################################################
class Element(notif.Notifier):
################################################################################
	''' Generic class for element, it contains element's position and project
	Others instance variable must be defined in subclass '''
	genericNumber = 0
	genericname = 'element'
	def __init__(self, project, pos):
		notif.Notifier.__init__(self)
		self.__connection={} # Contains all the Element's Connection [string : conn]
		self.__pos = pos
		self.__project = project
		self.__selected = False
		self._bmp = wx.Image("images/element/"+ self.genericName() +'.png').ConvertToBitmap()
		self._properties = {}
		self.doProp([('name',prop.StringFixedProperty,0)]) # Fills self._properties

	@classmethod
	def clearGN(klass):
		''' Clear genericNumber '''
		klass.genericNumber = 0	

	@classmethod
	def generateName(klass):
		''' Create the name that will be used in Project's elemList and nameList '''
		klass.genericNumber +=1
		return klass.genericname+str(klass.genericNumber)

	@classmethod
	def genericName(klass):
		return klass.genericname

	@classmethod
	def genericnumber(klass):
		return klass.genericNumber

	@property
	def bmp(self):
		return self._bmp

	@property
	def connection(self):
		return self.__connection

	@property
	def pos(self):
		return self.__pos

	@pos.setter
	def pos(self,p):
		assert type(p)==tuple
		self.__pos = p

	@property
	def project(self):
		return self.__project

	@property
	def properties(self):
		return self._properties

	@property
	def selected(self):
		return self.__selected

	@selected.setter
	def selected(self,val):
		assert type(val) == bool
		self.__selected = val

	def addConn(self, kind, dpos, namedic):
		''' Add a Connection in Element's Connection dictionnary.
		nameDic : fromElem = IN -> I+number, toElem = OUT -> O+number '''
		conn = Connection(self, kind, dpos)
		self.__connection[namedic] = conn
		return conn
	
	def clicked(self,point):
		return clicked(self,point)


	def connect(self, conn, conn2):
		''' Connect if conn2 is a Connection, deconnect if conn2 is None'''
		conn.link = conn2 # read link.setter DOC
		self.connectionChanged()

	def connectionChanged(self):
		''' Do notification in case of Connection's change'''
		for x in self._properties:
			self._properties[x].relax()		
		self.__project.doNotify()

	def deselect(self):
		self._bmp = wx.Image("images/element/"+ self.genericName() +'.png').ConvertToBitmap()
		self.__selected = False
		if self.__project.currentElem == self:
			self.__project.currentElem = None
		self.__project.doNotify()

	def detachFromAll(self):
		''' Deconnect myself with everybody I'm connected with AND deconnect everybody who was connected with me '''
		for x in self.__connection:
			self.__connection[x].link = None
		self.connectionChanged()

	def doProp(self, propList):
		''' Fill self._properties dictionnary in Element
		propList : [( 'name', 'propCategory', labelKey ), ( 'name', 'propCategory', labelKey ), ...]
		-> propCategory gives the type of property -- read Property DOC
		-> labelKey gives the order of apparition in propGrid '''
		for x in propList:
			if x[1] == 'floatfixed' :
				x = (x[0],prop.FloatFixedProperty, x[2])
			elif x[1] == 'stringfixed':
				x = (x[0],prop.StringFixedProperty, x[2])
			elif x[1] == 'floatvar':
				x = (x[0],prop.FloatVariableProperty, x[2])
			elif x[1] == 'stringvar' :
				x = (x[0],prop.StringVariableProperty, x[2])
			self._properties[x[0]] = x[1](self.project, x[0])
			self._properties[x[0]].labelKey  = x[2]
			self._properties[x[0]].wantNotif.add(self) # for Element's flash
			if x[0] == 'name':
				self._properties[x[0]].value = str(self.genericName())+str(self.genericnumber()+1) # generate automatic name

	def select(self):
		self._bmp = wx.Image("images/element/"+ self.genericName() +'select'+'.png').ConvertToBitmap()
		self.__selected = True
		self.__project.doNotify()


	## les fonction ci-apres doivent y rester pour cause d utilisation des fonctions precedentes
	def clear(self):
		self.detachFromAll()
		self.clearGN()
		self.__project.doNotify()

################################################################################
class State(Element): 
################################################################################
	''' State flash themselve to complete theyr properties '''
	genericNumber = 0
	genericname = 'state'

	#FLASH Equation
	equaState = equa.Equation()

	#Specific volume
	equaState.addEquation(V('specific volume')-equa.v_pT(V('pressure'),V('temperature')))
	equaState.addEquation(V('specific volume')-equa.v_ph(V('pressure'),V('enthalpy')))
	equaState.addEquation(V('specific volume')-equa.v_ps(V('pressure'),V('entropy')))

	#Temperature
	equaState.addEquation(V('temperature')-equa.T_px(V('pressure'),V('quality')))
	equaState.addEquation(V('temperature')-equa.T_ph(V('pressure'),V('enthalpy')))
	equaState.addEquation(V('temperature')-equa.T_hs(V('enthalpy'),V('entropy')))

	#Pressure
	equaState.addEquation(V('pressure')-equa.p_Tx(V('temperature'),V('quality')))
	equaState.addEquation(V('pressure')-equa.p_hs(V('enthalpy'),V('entropy')))

	#Enthalpy
	equaState.addEquation(V('enthalpy')-equa.h_pT(V('pressure'),V('temperature')))
	equaState.addEquation(V('enthalpy')-equa.h_ps(V('pressure'),V('entropy')))
	equaState.addEquation(V('enthalpy')-equa.h_px(V('pressure'),V('quality')))
	equaState.addEquation(V('enthalpy')-equa.h_Tx(V('temperature'),V('quality')))

	#Entropy
	equaState.addEquation(V('entropy')-equa.s_pT(V('pressure'),V('temperature')))
	equaState.addEquation(V('entropy')-equa.s_ph(V('pressure'),V('enthalpy')))

	#quality
	equaState.addEquation(V('quality')-equa.x_ph(V('pressure'),V('enthalpy')))
	equaState.addEquation(V('quality')-equa.x_ps(V('pressure'),V('entropy')))

	#Exergy
	equaState.addEquation(V('exergy')-V('enthalpy')+ \
	equa.RefVar('reference enthalpy',None)+ \
	equa.RefVar('reference temperature',None)*(V('entropy')-equa.RefVar('reference entropy',None)))
	equaState.addEquation(equa.CheckNullExergy(V('exergy')))

	#Reference (please refer to equa.py ifyou want to know howto build these equation)
	equaState.addEquation(equa.selfRefVar('reference enthalpy')-equa.selfRefVar('enthalpy'))
	equaState.addEquation(equa.selfRefVar('reference entropy')-equa.selfRefVar('entropy'))
	equaState.addEquation(equa.selfRefVar('reference temperature')-equa.selfRefVar('temperature'))
	equaState.addEquation(equa.selfRefVar('reference quality')-equa.selfRefVar('quality'))
	equaState.addEquation(equa.selfRefVar('reference pressure')-equa.selfRefVar('pressure'))
	equaState.addEquation(equa.CheckRef(V('reference state')))

	equaState.addEquation(equa.RefVar('reference pressure',None)-equa.p_Tx(equa.RefVar('reference temperature',None),equa.RefVar('reference quality',None)))
	equaState.addEquation(equa.RefVar('reference temperature',None)-equa.T_ph(equa.RefVar('reference pressure',None),equa.RefVar('reference enthalpy',None)))
	equaState.addEquation(equa.RefVar('reference temperature',None)-equa.T_hs(equa.RefVar('reference enthalpy',None),equa.RefVar('reference entropy',None)))
	equaState.addEquation(equa.RefVar('reference pressure',None)-equa.p_hs(equa.RefVar('reference enthalpy',None),equa.RefVar('reference entropy',None)))
	equaState.addEquation(equa.RefVar('reference enthalpy',None)-equa.h_px(equa.RefVar('reference pressure',None),equa.RefVar('reference quality',None)))
	equaState.addEquation(equa.RefVar('reference enthalpy',None)-equa.h_pT(equa.RefVar('reference pressure',None),equa.RefVar('reference temperature',None)))
	equaState.addEquation(equa.RefVar('reference enthalpy',None)-equa.h_ps(equa.RefVar('reference pressure',None),equa.RefVar('reference entropy',None)))
	equaState.addEquation(equa.RefVar('reference enthalpy',None)-equa.h_Tx(equa.RefVar('reference temperature',None),equa.RefVar('reference quality',None)))
	equaState.addEquation(equa.RefVar('reference entropy',None)-equa.s_pT(equa.RefVar('reference pressure',None),equa.RefVar('reference temperature',None)))
	equaState.addEquation(equa.RefVar('reference entropy',None)-equa.s_ph(equa.RefVar('reference pressure',None),equa.RefVar('reference enthalpy',None)))
	equaState.addEquation(equa.RefVar('reference quality',None)-equa.x_ph(equa.RefVar('reference pressure',None),equa.RefVar('reference enthalpy',None)))
	equaState.addEquation(equa.RefVar('reference quality',None)-equa.x_ps(equa.RefVar('reference pressure',None),equa.RefVar('reference entropy',None)))

	equaState.processTodo()

	def __init__(self,project,pos):
		Element.__init__(self,project,pos)
		self.conn1 = self.addConn('fromElem',(-10,6.5),'I1')
		self.conn2 = self.addConn('toElem',(78,6.5),'O1')
		self.__zone = (self.pos,(self.pos[0]+45,self.pos[1]+45))
		self.doProp( [ 	('fluid',prop.FluidProperty,1),\
				('specific volume',prop.FloatTellProperty,2),\
				('temperature','floatvar',3),\
				('pressure','floatvar',4),\
				('quality',prop.QualityProperty,5),\
				('entropy','floatvar',6),\
				('enthalpy','floatvar',7),\
				('exergy','floatvar',8),\
				('flow','floatvar',9),\
				('reference temperature','floatvar',10),\
				('reference quality',prop.QualityProperty,11),\
				('reference pressure','floatvar',12),\
				('reference entropy','floatvar',13),\
				('reference enthalpy','floatvar',14),\
				('reference state',prop.BoolProperty,15)] )
		#Le quality est la proportion de vapeur dans le melange eau-vapeur.
		#Specific volume = masse volumique [m ^ 3/kg]


	@property
	def zone(self):
		''' The zone contains the area of the State's picture in GUI panel'''
		self.__zone = (self.pos,(self.pos[0]+80,self.pos[1]+23))
		return self.__zone

	@property
	def bmp(self):
		if self.selected == False:
			if self.connection['I1'].link == None and self.connection['O1'].link == None:
				self._bmp = wx.Image("images/element/statenoconn.png").ConvertToBitmap()
			elif self.connection['I1'].link == None:
				self._bmp = wx.Image("images/element/stateO1conn.png").ConvertToBitmap()
			elif self.connection['O1'].link == None:
				self._bmp = wx.Image("images/element/stateI1conn.png").ConvertToBitmap()
			elif self.connection['I1'].link != None and self.connection['O1'].link != None:
				self._bmp = wx.Image("images/element/stateconn.png").ConvertToBitmap()
		else :
			if self.connection['I1'].link == None and self.connection['O1'].link == None:
				self._bmp = wx.Image("images/element/statenoconnselect.png").ConvertToBitmap()
			elif self.connection['I1'].link == None:
				self._bmp = wx.Image("images/element/stateO1connselect.png").ConvertToBitmap()
			elif self.connection['O1'].link == None:
				self._bmp = wx.Image("images/element/stateI1connselect.png").ConvertToBitmap()
			elif self.connection['I1'].link != None and self.connection['O1'].link != None:
				self._bmp = wx.Image("images/element/stateconnselect.png").ConvertToBitmap()
		return self._bmp

	def select(self):
		if self.connection['I1'].link == None and self.connection['O1'].link == None:
			self._bmp = wx.Image("images/element/statenoconnselect.png").ConvertToBitmap()
		elif self.connection['I1'].link == None:
			self._bmp = wx.Image("images/element/stateO1connselect.png").ConvertToBitmap()
		elif self.connection['O1'].link == None:
			self._bmp = wx.Image("images/element/stateI1connselect.png").ConvertToBitmap()
		elif self.connection['I1'].link != None and self.connection['O1'].link != None:
			self._bmp = wx.Image("images/element/stateconnselect.png").ConvertToBitmap()
		self.selected = True
		self.project.doNotify()


	def update(self):
		''' Check in Element's equation if it can find solution and tell them'''
		elemUpdate(self,State.equaState.equationList)

allElem.add((State.genericname, State)) # Add State



################################################################################
class Transfo(Element):
################################################################################
	''' Transformation take variable from a state to transform it with theyr
	equation, and return result to the next state '''
	genericNumber = 0
	genericname = 'transfo'

	def __init__(self,project,pos):
		Element.__init__(self,project,pos)
		self._watchedConnectionVariables=set() 

	@property
	def watchedConnectionVariables(self):
		''' TODO : DOC '''
		return self._watchedConnectionVariables

	def connectionChanged(self):
		''' Do notification in case of Connection's change'''
		for x in self.watchedConnectionVariables:
			x.wantNotif.remove(self)
		self.watchedConnectionVariables.clear()
		for x in self.connection:
			y = self.connection[x].link
			if y != None:
				for z in y.elem.properties:
					y.elem.properties[z].wantNotif.add(self)
					self.watchedConnectionVariables.add(y.elem.properties[z]) # To be removed from wantNotif later

	def update(self):
		''' Check in Element's equation if it can find solution and tell them'''
		pass


################################################################################
class TransfoInOut(Transfo):
################################################################################
	''' Specific class for transformation with one in and one out. '''
	#Equations
	equaTransfoInOut = equa.Equation()
	equaTransfoInOut.addEquation(equa.PropagateFluid(V('fluid','I1'),V('fluid','O1')))
	equaTransfoInOut.addEquation(V('flow','I1')-V('flow','O1'))
	equaTransfoInOut.addEquation(equa.RefVar('reference temperature','I1')-equa.RefVar('reference temperature','O1'))
	equaTransfoInOut.addEquation(equa.RefVar('reference entropy','I1')-equa.RefVar('reference entropy','O1'))
	equaTransfoInOut.addEquation(equa.RefVar('reference enthalpy','I1')-equa.RefVar('reference enthalpy','O1'))
	equaTransfoInOut.addEquation(equa.RefVar('reference quality','I1')-equa.RefVar('reference quality','O1'))
	equaTransfoInOut.addEquation(equa.RefVar('reference pressure','I1')-equa.RefVar('reference pressure','O1'))
	equaTransfoInOut.processTodo()

	def __init__(self,project,pos):
		Transfo.__init__(self,project,pos)

################################################################################
class Pump(TransfoInOut): 
################################################################################
	''' Isentropic pump'''
	genericNumber = 0
	genericname = 'pump'

	#Equations
	equaPump = equa.Equation()

	#equaPump.addEquation(V('pressure','O1')/V('pressure','I1') -V('pressure ratio'))
	equaPump.addEquation(V('pressure','O1')-V('pressure','I1') -V('pressure delta'))
	equaPump.addEquation(V('polytropic efficiency')-V('specific volume','I1')* \
	(V('pressure','O1')-V('pressure','I1'))/((V('enthalpy','O1')-V('enthalpy','I1')))) # eta_pi = v dp / dh
	#We use specific volume from I1 fluid. BE CAREFULL specific volume will change during the process.
	#User must check if the variation of specific volume doesn't matter !

	#Energy By convention : when fluid recieve energy, work will be positive, when fluid give energy, work is negative
	equaPump.addEquation(V('work')+V('enthalpy','I1')-V('enthalpy','O1'))

	#Power
	equaPump.addEquation(V('power lost')-V('flow','I1')*V('work')/V('mechanical efficiency'))
	equaPump.processTodo()

	def __init__(self,project,pos):
		TransfoInOut.__init__(self,project,pos)
		self.__zone = (self.pos,(self.pos[0]+75,self.pos[1]+55))
		self.conn1 = self.addConn('fromElem',(70,22),'I1')
		self.conn2 = self.addConn('toElem',(-5,22),'O1')
		self.doProp([('pressure delta','floatvar',1),\
			('polytropic efficiency','floatvar',2),\
			('mechanical efficiency','floatvar',3),\
			('work','floatvar',4),\
			('power lost','floatvar',5)])
		#logger.info('Pump created')

	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+75,self.pos[1]+55))
		return self.__zone
	
	def update(self):
		elemUpdate(self,TransfoInOut.equaTransfoInOut.equationList)
		elemUpdate(self,Pump.equaPump.equationList)

allElem.add((Pump.genericname, Pump))


################################################################################
class Condenser(TransfoInOut): 
################################################################################
	''' Isobar and isotherm condenser that impose saturation for fluid out '''
	genericNumber = 0
	genericname = 'condenser'

	#Equation
	equaCondenser = equa.Equation()

	#Hypothesis : condenser is isobar , impose saturation
	equaCondenser.addEquation(V('pressure','I1')-V('pressure','O1'))
	equaCondenser.addEquation(V('quality','O1'))
	equaCondenser.addEquation(V('temperature','O1')-V('temperature out'))
	equaCondenser.addEquation(V('energy lost')+V('enthalpy','I1')-V('enthalpy','O1'))
	equaCondenser.addEquation(V('power lost')-V('flow','I1')*V('energy lost'))
	equaCondenser.processTodo()

	def __init__(self,project,pos):
		TransfoInOut.__init__(self,project,pos)
		self.__zone = (self.pos,(self.pos[0]+80,self.pos[1]+50))
		self.conn1 = self.addConn('fromElem',(33,0),'I1')
		self.conn2 = self.addConn('toElem',(33,45),'O1')
		self.doProp([('temperature out','floatvar',1),\
			('power lost','floatvar',2),\
			('energy lost','floatvar',3)])
		#logger.info('Condenser created')

	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+80,self.pos[1]+50))
		return self.__zone

	def update(self):
		elemUpdate(self,TransfoInOut.equaTransfoInOut.equationList)
		elemUpdate(self,Condenser.equaCondenser.equationList)

allElem.add((Condenser.genericname, Condenser))


################################################################################
class Boiler(TransfoInOut): 
################################################################################
	''' Isobar boiler with no combustion compute'''
	genericNumber = 0
	genericname = 'boiler'

	#Equation
	equaBoiler = equa.Equation()

	#Hypothesis : boiler is isobar
	equaBoiler.addEquation(V('temperature','O1')-V('temperature out'))
	equaBoiler.addEquation(V('pressure','O1')-V('pressure','I1'))
	equaBoiler.addEquation(V('power')-V('flow','I1')*(V('enthalpy','I1')-V('enthalpy','O1'))/V('efficiency'))
	equaBoiler.addEquation(V('power lost')-(K(1)-V('efficiency'))*V('power'))
	equaBoiler.processTodo()

	def __init__(self,project,pos):
		TransfoInOut.__init__(self,project,pos)
		self.__zone = (self.pos,(self.pos[0]+75,self.pos[1]+110))
		self.conn1 = self.addConn('fromElem',(32.5,105),'I1')
		self.conn2 = self.addConn('toElem',(33,0),'O1')
		self.doProp([('temperature out','floatvar',1),\
			('efficiency','floatvar',2),\
			('power','floatvar',3),\
			('power lost','floatvar',4)])
		#logger.info('Boiler created')

	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+75,self.pos[1]+110))
		return self.__zone

	def update(self):
		elemUpdate(self,TransfoInOut.equaTransfoInOut.equationList)
		elemUpdate(self,Boiler.equaBoiler.equationList)

allElem.add((Boiler.genericname, Boiler))


################################################################################
class Turbine(TransfoInOut): 
################################################################################
	''' Turbine '''
	genericNumber = 0
	genericname = 'turbine'

	#Equation
	equaTurbine = equa.Equation()

	#Hypothesis
	equaTurbine.addEquation(V('isentropic efficiency')- \
	 ((V('enthalpy','I1')-V('enthalpy','O1'))/ \
	(V('enthalpy','I1')-equa.h_ps(V('pressure','O1'),V('entropy','I1')))))

	equaTurbine.addEquation(V('enthalpy','I1')- \
	(V('isentropic efficiency')*equa.h_ps(V('pressure','O1'), \
	V('entropy','I1'))-V('enthalpy','O1'))/(V('isentropic efficiency')-K(1)))
	#Besoin des deux equations en cas de redondance des variables dans les equations.

	#Energy convention : when fluid recieve energy, work will be positive, when fluid give energy, work is negative
	equaTurbine.addEquation(V('work')+V('enthalpy','I1')-V('enthalpy','O1'))
	equaTurbine.addEquation(V('power')-V('mechanical efficiency')*V('flow','I1')*(V('work')))
	equaTurbine.addEquation(V('power lost')-(K(1)-V('mechanical efficiency'))*V('flow','I1')*(V('work')))
	equaTurbine.processTodo()

	def __init__(self,project,pos):
		TransfoInOut.__init__(self,project,pos)
		self.__zone = (self.pos,(self.pos[0]+75,self.pos[1]+78))
		self.conn1 = self.addConn('fromElem',(0,27),'I1')
		self.conn2 = self.addConn('toElem',(63,70),'O1')
		self.doProp([('isentropic efficiency','floatvar',1),\
			('mechanical efficiency','floatvar',2),\
			('work','floatvar',3),\
			('power','floatvar',4),\
			('power lost','floatvar',5)])
		#logger.info('Turbine created')

	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+75,self.pos[1]+78))
		return self.__zone

	def update(self):
		elemUpdate(self,TransfoInOut.equaTransfoInOut.equationList)
		elemUpdate(self,Turbine.equaTurbine.equationList)

allElem.add((Turbine.genericname, Turbine))


################################################################################
class Delta(TransfoInOut):
################################################################################
	''' Delta allow to impose delat of pressure, temperature,enthalpy,
	entropy and exergy between In and Out '''
	genericNumber = 0
	genericname = 'delta'

	#Equation
	equaDelta = equa.Equation()
	equaDelta.addEquation(V('delta temperature')+V('temperature','I1')-V('temperature','O1'))
	equaDelta.addEquation(V('delta pressure')+V('pressure','I1')-V('pressure','O1'))
	equaDelta.addEquation(V('delta enthalpy')+V('enthalpy','I1')-V('enthalpy','O1'))
	equaDelta.addEquation(V('delta entropy')+V('entropy','I1')-V('entropy','O1'))
	equaDelta.addEquation(V('delta exergy')+V('exergy','I1')-V('exergy','O1'))
	equaDelta.processTodo()

	def __init__(self,project,pos):
		TransfoInOut.__init__(self,project,pos)
		self.__zone = (self.pos,(self.pos[0]+50,self.pos[1]+50))	
		self.conn1 = self.addConn('toElem',(-5,20),'O1')
		self.conn2 = self.addConn('fromElem',(45,20),'I1')
		self.doProp( [('delta temperature','floatvar',1),\
			('delta pressure','floatvar',2),\
			('delta enthalpy','floatvar',3),\
			('delta entropy','floatvar',4),\
			('delta exergy','floatvar',5)] )
		#logger.info('Delta created')

	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+50,self.pos[1]+50))
		return self.__zone

	def update(self):
		elemUpdate(self,TransfoInOut.equaTransfoInOut.equationList)
		elemUpdate(self,Delta.equaDelta.equationList)

allElem.add((Delta.genericname, Delta))


################################################################################
class ExtractionTurbine(Transfo):
################################################################################
	''' Turbine with one extraction set at the middle of enthalpy '''
	genericNumber = 0
	genericname = 'extraction turbine'
	equaExTurb = equa.Equation()

	#Pour la sortie O1 normale:
	equaExTurb.addEquation(equa.PropagateFluid(V('fluid','I1'),V('fluid','O1')))
	equaExTurb.addEquation(equa.RefVar('reference temperature','I1')-equa.RefVar('reference temperature','O1'))
	equaExTurb.addEquation(equa.RefVar('reference entropy','I1')-equa.RefVar('reference entropy','O1'))
	equaExTurb.addEquation(equa.RefVar('reference enthalpy','I1')-equa.RefVar('reference enthalpy','O1'))
	equaExTurb.addEquation(equa.RefVar('reference quality','I1')-equa.RefVar('reference quality','O1'))
	equaExTurb.addEquation(equa.RefVar('reference pressure','I1')-equa.RefVar('reference pressure','O1'))

	#Besoin des deux equations car redondance des variables dans les equations.
	equaExTurb.addEquation(V('isentropic efficiency')-((V('enthalpy','I1')-V('enthalpy','O1'))/(V('enthalpy','I1')-equa.h_ps(V('pressure','O1'),V('entropy','I1')))))
	equaExTurb.addEquation(V('enthalpy','I1')-(V('isentropic efficiency')*equa.h_ps(V('pressure','O1'),V('entropy','I1'))-V('enthalpy','O1'))/(V('isentropic efficiency')-K(1))) 

	#Pour la sortie O2 soutiree:
	equaExTurb.addEquation(equa.PropagateFluid(V('fluid','I1'),V('fluid','O2')))
	equaExTurb.addEquation(equa.RefVar('reference temperature','I1')-equa.RefVar('reference temperature','O2'))
	equaExTurb.addEquation(equa.RefVar('reference entropy','I1')-equa.RefVar('reference entropy','O2'))
	equaExTurb.addEquation(equa.RefVar('reference enthalpy','I1')-equa.RefVar('reference enthalpy','O2'))
	equaExTurb.addEquation(equa.RefVar('reference quality','I1')-equa.RefVar('reference quality','O2'))
	equaExTurb.addEquation(equa.RefVar('reference pressure','I1')-equa.RefVar('reference pressure','O2'))

	#Besoin des deux equations car redondance des variables dans les equations.
	equaExTurb.addEquation(V('isentropic efficiency')-((V('enthalpy','I1')-V('enthalpy','O2'))/(V('enthalpy','I1')-equa.h_ps(V('pressure','O2'),V('entropy','I1')))))
	equaExTurb.addEquation(V('enthalpy','I1')-(V('isentropic efficiency')*equa.h_ps(V('pressure','O2'),V('entropy','I1'))-V('enthalpy','O2'))/(V('isentropic efficiency')-K(1)))
	#On trouve la pression a partir d'un etat isentropique,qui est la meme que l'etat normal. : h7s obtenu avec l'equation du rendement:   H3-(H3-H7(i))/RendIsTu, s7s = sI1
	equaExTurb.addEquation(V('pressure','O2')-equa.p_hs((V('enthalpy','I1')-((V('enthalpy','I1')-V('enthalpy','O2'))/V('isentropic efficiency'))) ,V('entropy','I1')))
	


	#L'enthalpie de la fraction soutiree est par hypothese au milieu entre celle de l'entree de celle de sortie
	equaExTurb.addEquation(V('enthalpy','O2')-V('enthalpy','I1')/K(2)-V('enthalpy','O1')/K(2))
	#Flow
	equaExTurb.addEquation(V('flow','I1')-V('flow','O1')-V('flow','O2'))
	# Attention aux conventions sur le signe du travail, Wmt*FlowO1 = (h3-h4)*FlowO1+(h3-h7)*FlowO2
	equaExTurb.addEquation(V('work')*V('flow','I1') + (V('enthalpy','I1')-V('enthalpy','O1'))* V('flow','O1') + (V('enthalpy','I1')-V('enthalpy','O2')) * V('flow','O2') )

	#Equation similaire au travail, sauf qu'on ne multiplie plus le travail par flow
	equaExTurb.addEquation(V('power') + V('work')*V('flow','I1') )

	equaExTurb.addEquation(V('power lost')-(K(1)-V('mechanical efficiency'))*V('flow','I1')*(V('work')))

	equaExTurb.processTodo()

	def __init__(self, project,pos):
		Transfo.__init__(self, project,pos)
		self.__zone = (self.pos,(self.pos[0]+75,self.pos[1]+78))
		self.conn1 = self.addConn('toElem',(63,70),'O1') #first out = normal way
		self.conn2 = self.addConn('toElem',(27,58),'O2') #second out = extract way
		self.conn3 = self.addConn('fromElem',(0,27),'I1')
		self.doProp([('isentropic efficiency','floatvar',1),\
			('mechanical efficiency','floatvar',2),\
			('work','floatvar',3),\
			('power','floatvar',4),\
			('power lost','floatvar',5)])

	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+75,self.pos[1]+78))
		return self.__zone

	def update(self):
		elemUpdate(self,ExtractionTurbine.equaExTurb.equationList)

allElem.add((ExtractionTurbine.genericname, ExtractionTurbine))


################################################################################
class HeaterCondenser(Transfo):
################################################################################
	'''  Heat exchanger contercurrent where the hot fluid get out at saturation '''
	#User must be carefull when he connect Hot fluid and Cold fluid!
	#Hot fluid is line 1
	#Cold fluid is line 2
	genericNumber = 0
	genericname = 'heater condenser' #page 70
	equaHeaterCond = equa.Equation()
	#Hypothesis: exchanger condenser set titre at 0 for hot fluid, is isobar
	#Line 1 Hot fluid:
	equaHeaterCond.addEquation(equa.PropagateFluid(V('fluid','I1'),V('fluid','O1')))
	equaHeaterCond.addEquation(V('flow','I1')-V('flow','O1'))
	equaHeaterCond.addEquation(equa.RefVar('reference temperature','I1')-equa.RefVar('reference temperature','O1'))
	equaHeaterCond.addEquation(equa.RefVar('reference entropy','I1')-equa.RefVar('reference entropy','O1'))
	equaHeaterCond.addEquation(equa.RefVar('reference enthalpy','I1')-equa.RefVar('reference enthalpy','O1'))
	equaHeaterCond.addEquation(equa.RefVar('reference quality','I1')-equa.RefVar('reference quality','O1'))
	equaHeaterCond.addEquation(equa.RefVar('reference pressure','I1')-equa.RefVar('reference pressure','O1'))
	equaHeaterCond.addEquation(V('quality','O1')-K(0))
	equaHeaterCond.addEquation(V('pressure','I1')-V('pressure','O1'))
	#Line 2 Cold fluid: hot part of these line = cold part of line 1 - delta temperature between the two fluid at the begining of line 2 (= pincement)
	equaHeaterCond.addEquation(equa.PropagateFluid(V('fluid','I2'),V('fluid','O2')))
	equaHeaterCond.addEquation(V('flow','I2')-V('flow','O2'))
	equaHeaterCond.addEquation(equa.RefVar('reference temperature','I2')-equa.RefVar('reference temperature','O2'))
	equaHeaterCond.addEquation(equa.RefVar('reference entropy','I2')-equa.RefVar('reference entropy','O2'))
	equaHeaterCond.addEquation(equa.RefVar('reference enthalpy','I2')-equa.RefVar('reference enthalpy','O2'))
	equaHeaterCond.addEquation(equa.RefVar('reference quality','I2')-equa.RefVar('reference quality','O2'))
	equaHeaterCond.addEquation(equa.RefVar('reference pressure','I2')-equa.RefVar('reference pressure','O2'))
	equaHeaterCond.addEquation(V('pressure','I2')-V('pressure','O2'))
	equaHeaterCond.addEquation(V('delta T')+V('temperature','O1')-V('temperature','O2')) #faire un schema d'un echangeur contrecourant

	#Equation for flow
	equaHeaterCond.addEquation(V('flow','I1')*(V('enthalpy','I1')-V('enthalpy','O1'))-V('flow','I2')*(V('enthalpy','O2')-V('enthalpy','I2')))
	equaHeaterCond.addEquation(V('delta flow')+V('flow','I1')-V('flow','I2'))

	equaHeaterCond.processTodo()


	def __init__(self,project,pos):
		Transfo.__init__(self,project,pos)
		self.__zone = (self.pos,(self.pos[0]+60,self.pos[1]+60))
		self.conn1 = self.addConn('fromElem',(25,0),'I1')
		self.conn2 = self.addConn('toElem',(25,45),'O1')
		self.conn3 = self.addConn('fromElem',(50,23),'I2')
		self.conn4 = self.addConn('toElem',(0,23),'O2')
		self.doProp([('delta T','floatvar',1),\
			('delta flow','floatvar',2)])

	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+60,self.pos[1]+60))
		return self.__zone

	def update(self):
		elemUpdate(self,HeaterCondenser.equaHeaterCond.equationList)

allElem.add((HeaterCondenser.genericname, HeaterCondenser))

################################################################################
class Subcooler(Transfo):
################################################################################
	''' Heat exchanger contercurrent with specific hypothesis and no 
	LMTD calculation  '''
	#User must be carefull when he connect Hot fluid and Cold fluid!
	#Hot fluid is line 1
	#Cold fluid is line 2
	genericNumber = 0
	genericname = 'subcooler' #page 70
	equaSC = equa.Equation()
	#Hypothesis: exchanger condenser set titre at 0 for hot fluid, is isobar
	#Line 1 Hot fluid:
	equaSC.addEquation(equa.PropagateFluid(V('fluid','I1'),V('fluid','O1')))
	equaSC.addEquation(V('flow','I1')-V('flow','O1'))
	equaSC.addEquation(equa.RefVar('reference temperature','I1')-equa.RefVar('reference temperature','O1'))
	equaSC.addEquation(equa.RefVar('reference entropy','I1')-equa.RefVar('reference entropy','O1'))
	equaSC.addEquation(equa.RefVar('reference enthalpy','I1')-equa.RefVar('reference enthalpy','O1'))
	equaSC.addEquation(equa.RefVar('reference quality','I1')-equa.RefVar('reference quality','O1'))
	equaSC.addEquation(equa.RefVar('reference pressure','I1')-equa.RefVar('reference pressure','O1'))
	equaSC.addEquation(V('pressure','I1')-V('pressure','O1'))
	#Line 2 Cold fluid: hot part of these line = cold part of line 1 - delta temperature between the two fluid at the begining of line 2 (= pincement)
	equaSC.addEquation(equa.PropagateFluid(V('fluid','I2'),V('fluid','O2')))
	equaSC.addEquation(V('flow','I2')-V('flow','O2'))
	equaSC.addEquation(equa.RefVar('reference temperature','I2')-equa.RefVar('reference temperature','O2'))
	equaSC.addEquation(equa.RefVar('reference entropy','I2')-equa.RefVar('reference entropy','O2'))
	equaSC.addEquation(equa.RefVar('reference enthalpy','I2')-equa.RefVar('reference enthalpy','O2'))
	equaSC.addEquation(equa.RefVar('reference quality','I2')-equa.RefVar('reference quality','O2'))
	equaSC.addEquation(equa.RefVar('reference pressure','I2')-equa.RefVar('reference pressure','O2'))
	equaSC.addEquation(V('pressure','I2')-V('pressure','O2'))
	equaSC.addEquation(V('delta T')+V('temperature','I2')-V('temperature','O1')) #faire un schema d'un echangeur contrecourant
	#Equation for flow
	equaSC.addEquation(V('flow','I1')*(V('enthalpy','I1')-V('enthalpy','O1'))-V('flow','I2')*(V('enthalpy','O2')-V('enthalpy','I2')))
	equaSC.addEquation(V('delta flow')+V('flow','I1')-V('flow','I2'))
	equaSC.processTodo()
	equaSC.processTodo()


	def __init__(self,project,pos):
		Transfo.__init__(self,project,pos)
		self.__zone = (self.pos,(self.pos[0]+60,self.pos[1]+60))
		self.conn1 = self.addConn('fromElem',(25,0),'I1')
		self.conn2 = self.addConn('toElem',(25,45),'O1')
		self.conn3 = self.addConn('fromElem',(50,23),'I2')
		self.conn4 = self.addConn('toElem',(0,23),'O2')
		self.doProp([('delta T','floatvar',1),\
			('delta flow','floatvar',2)])

	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+60,self.pos[1]+60))
		return self.__zone

	def update(self):
		elemUpdate(self,Subcooler.equaSC.equationList)

allElem.add((Subcooler.genericname, Subcooler))

################################################################################
class Valve(TransfoInOut):
################################################################################
	''' Valve'''
	genericNumber = 0
	genericname = 'valve'

	#Equation
	equaValve = equa.Equation()
	#hypothesis: adiabatic+no work = isenthalpic
	equaValve.addEquation(V('enthalpy','I1')-V('enthalpy','O1'))
	equaValve.addEquation(V('pressure','O1')-V('pressure','I1')-V('delta pressure'))
	equaValve.processTodo()

	def __init__(self, project,pos):
		TransfoInOut.__init__(self, project,pos)
		self.__zone = (self.pos,(self.pos[0]+76,self.pos[1]+27))
		self.conn1 = self.addConn('toElem',(0,9),'O1')
		self.conn2 = self.addConn('fromElem',(74,9),'I1')
		self.doProp([('delta pressure','floatvar',1)])


	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+76,self.pos[1]+27))
		return self.__zone

	def update(self):
		elemUpdate(self,TransfoInOut.equaTransfoInOut.equationList)
		elemUpdate(self,Valve.equaValve.equationList)


allElem.add((Valve.genericname, Valve))

################################################################################
class ExtractCondenser(Transfo):
################################################################################
	'''Condenser with two In and one Out'''
	genericNumber = 0
	genericname = 'extract condenser'
	
	#Equation
	eq = equa.Equation()
	eq.addEquation(equa.RefVar('reference temperature','I1')-equa.RefVar('reference temperature','O1'))
	eq.addEquation(equa.RefVar('reference entropy','I1')-equa.RefVar('reference entropy','O1'))
	eq.addEquation(equa.RefVar('reference enthalpy','I1')-equa.RefVar('reference enthalpy','O1'))
	eq.addEquation(equa.RefVar('reference quality','I1')-equa.RefVar('reference quality','O1'))
	eq.addEquation(equa.RefVar('reference pressure','I1')-equa.RefVar('reference pressure','O1'))
	eq.addEquation(equa.RefVar('reference temperature','I2')-equa.RefVar('reference temperature','O1'))
	eq.addEquation(equa.RefVar('reference entropy','I2')-equa.RefVar('reference entropy','O1'))
	eq.addEquation(equa.RefVar('reference enthalpy','I2')-equa.RefVar('reference enthalpy','O1'))
	eq.addEquation(equa.RefVar('reference quality','I2')-equa.RefVar('reference quality','O1'))
	eq.addEquation(equa.RefVar('reference pressure','I2')-equa.RefVar('reference pressure','O1'))
	#hypothesis:
	eq.addEquation(V('pressure','I1')-V('pressure','O1'))
	eq.addEquation(V('pressure','I2')-V('pressure','O1'))
	eq.addEquation(V('temperature','I1')-V('temperature','O1'))
	eq.addEquation(V('temperature','I2')-V('temperature','O1'))
	eq.addEquation(V('quality','O1')-K(0))
	eq.addEquation(equa.PropagateFluid(V('fluid','I1'),V('fluid','O1')))
	eq.addEquation(equa.PropagateFluid(V('fluid','I2'),V('fluid','O1')))
	eq.addEquation(V('flow','I1')+V('flow','I2')-V('flow','O1'))
	# P =  (hI1-h4)*flowI1 + (hI2-h4)*flowI2
	eq.addEquation(V('power lost')-(V('enthalpy','O1')-V('enthalpy','I1'))*V('flow','I1')-(V('enthalpy','O1')-V('enthalpy','I2'))*V('flow','I2'))
	eq.addEquation(V('temperature out')-V('temperature','O1'))
	eq.processTodo()

	def __init__(self,project,pos):
		Transfo.__init__(self,project,pos)
		self.__zone = (self.pos,(self.pos[0]+80,self.pos[1]+50))
		self.conn1 = self.addConn('fromElem',(33,0),'I1')
		self.conn1 = self.addConn('fromElem',(0,20),'I2')
		self.conn2 = self.addConn('toElem',(33,45),'O1')
		self.doProp([('temperature out','floatvar',1),\
			('power lost','floatvar',2)])

	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+80,self.pos[1]+50))
		return self.__zone

	def update(self):
		elemUpdate(self,ExtractCondenser.eq.equationList)

allElem.add((ExtractCondenser.genericname, ExtractCondenser))
################################################################################
class Mixer(Transfo):
################################################################################
	''' Mix two fluid with the SAME properties except for flows'''
	genericNumber = 0
	genericname = 'mixer'

	#Equation
	eq = equa.Equation()
	eq.addEquation(equa.RefVar('reference temperature','I1')-equa.RefVar('reference temperature','O1'))
	eq.addEquation(equa.RefVar('reference entropy','I1')-equa.RefVar('reference entropy','O1'))
	eq.addEquation(equa.RefVar('reference enthalpy','I1')-equa.RefVar('reference enthalpy','O1'))
	eq.addEquation(equa.RefVar('reference quality','I1')-equa.RefVar('reference quality','O1'))
	eq.addEquation(equa.RefVar('reference pressure','I1')-equa.RefVar('reference pressure','O1'))
	eq.addEquation(equa.RefVar('reference temperature','I2')-equa.RefVar('reference temperature','O1'))
	eq.addEquation(equa.RefVar('reference entropy','I2')-equa.RefVar('reference entropy','O1'))
	eq.addEquation(equa.RefVar('reference enthalpy','I2')-equa.RefVar('reference enthalpy','O1'))
	eq.addEquation(equa.RefVar('reference quality','I2')-equa.RefVar('reference quality','O1'))
	eq.addEquation(equa.RefVar('reference pressure','I2')-equa.RefVar('reference pressure','O1'))

	#hypothesis : isobare and SAME fluid
	eq.addEquation(equa.PropagateFluid(V('fluid','I1'),V('fluid','O1')))
	eq.addEquation(equa.PropagateFluid(V('fluid','I2'),V('fluid','O1')))
	eq.addEquation(V('pressure','I1')-V('pressure','O1'))
	eq.addEquation(V('pressure','I2')-V('pressure','O1'))
	#Bilan de masse
	eq.addEquation(V('flow','O1')-V('flow','I1')-V('flow','I2'))
	#Bilan d'energie
	eq.addEquation(V('enthalpy','O1')-(V('flow','I1')*V('enthalpy','I1')+V('flow','I2')*V('enthalpy','I2'))/V('flow','O1'))
	eq.processTodo()

	def __init__(self, project,pos):
		Transfo.__init__(self, project,pos)
		self.__zone = (self.pos,(self.pos[0]+76,self.pos[1]+27))
		self.conn1 = self.addConn('toElem',(0,8),'O1')
		self.conn2 = self.addConn('fromElem',(74,-3),'I1')
		self.conn3 = self.addConn('fromElem',(74,21),'I2')


	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+76,self.pos[1]+27))
		return self.__zone

	def update(self):
		elemUpdate(self,Mixer.eq.equationList)


allElem.add((Mixer.genericname, Mixer))


################################################################################
class Demixer(Transfo):
################################################################################
	''' Split fluid in two '''
	genericNumber = 0
	genericname = 'demixer'

	#Equation
	eq = equa.Equation()
	eq.addEquation(equa.RefVar('reference temperature','I1')-equa.RefVar('reference temperature','O1'))
	eq.addEquation(equa.RefVar('reference entropy','I1')-equa.RefVar('reference entropy','O1'))
	eq.addEquation(equa.RefVar('reference enthalpy','I1')-equa.RefVar('reference enthalpy','O1'))
	eq.addEquation(equa.RefVar('reference quality','I1')-equa.RefVar('reference quality','O1'))
	eq.addEquation(equa.RefVar('reference pressure','I1')-equa.RefVar('reference pressure','O1'))
	eq.addEquation(equa.RefVar('reference temperature','I1')-equa.RefVar('reference temperature','O2'))
	eq.addEquation(equa.RefVar('reference entropy','I1')-equa.RefVar('reference entropy','O2'))
	eq.addEquation(equa.RefVar('reference enthalpy','I1')-equa.RefVar('reference enthalpy','O2'))
	eq.addEquation(equa.RefVar('reference quality','I1')-equa.RefVar('reference quality','O2'))
	eq.addEquation(equa.RefVar('reference pressure','I1')-equa.RefVar('reference pressure','O2'))

	#Hypothesis :
	eq.addEquation(equa.PropagateFluid(V('fluid','I1'),V('fluid','O1')))
	eq.addEquation(V('flow','I1')-V('flow','O1')-V('flow','O2'))
	eq.addEquation(V('flow','I1')*V('fraction flow')-V('flow','O1'))
	eq.addEquation(V('flow','I1')*(K(1)-V('fraction flow'))-V('flow','O1'))
	eq.processTodo()

	def __init__(self, project,pos):
		Transfo.__init__(self, project,pos)
		self.__zone = (self.pos,(self.pos[0]+76,self.pos[1]+27))
		self.conn1 = self.addConn('toElem',(0,-3),'O1')
		self.conn2 = self.addConn('toElem',(0,21),'O2')
		self.conn3 = self.addConn('fromElem',(74,8),'I1')
		self.doProp = ([('fraction flow','floatvar',1)])

	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+76,self.pos[1]+27))
		return self.__zone

	def update(self):
		elemUpdate(self,Demixer.eq.equationList)

allElem.add((Demixer.genericname, Demixer))

################################################################################
class Compressor(TransfoInOut):
################################################################################
	genericNumber = 0
	genericname = 'compressor'

	#Equation
	eq = equa.Equation()
	#hypothesys:
	#specific heat is the same for all the transformation

	eq.addEquation((V('pressure','O1')/V('pressure','I1'))**((V('gamma')-K(1))/(V('gamma')*V('polytropic efficiency')))-V('temperature','O1')/V('temperature','I1'))
	eq.addEquation(V('pressure','O1')/V('pressure','I1')-V('pressure ratio'))
	eq.addEquation(V('power lost')-V('flow','I1')*(V('enthalpy','I1')-V('enthalpy','O1')))

	eq.processTodo()

	def __init__(self,project,pos):
		TransfoInOut.__init__(self,project,pos)
		self.__zone = (self.pos,(self.pos[0]+75,self.pos[1]+55))
		self.conn1 = self.addConn('fromElem',(70,22),'I1')
		self.conn2 = self.addConn('toElem',(-5,22),'O1')
		self.doProp([('pressure ratio','floatvar',1),\
			('polytropic efficiency','floatvar',2),\
			('gamma','floatvar',3),\
			('power lost','floatvar',4)])

	@property
	def zone(self):
		self.__zone = (self.pos,(self.pos[0]+75,self.pos[1]+55))
		return self.__zone

	def update(self):
		elemUpdate(self,TransfoInOut.equaTransfoInOut.equationList)
		elemUpdate(self,Compressor.eq.equationList)

allElem.add((Compressor.genericname,Compressor))


