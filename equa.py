import math
import fluid
import prop


def V(x,y = None):
	if y == None:
		return selfVar(x)
	else:
		return Var(x,y)


def matchProperties(element,name):
	'''return matched property in given element'''
	if element.properties[name] != None :
		return element.properties[name]
	else :
		return None


def evalEquation(equation,element):
	'''Evaluate one equation from an element and return tuple with (property,value,whoHelp)
	property and not variable ! Each property is unique and linked the element which contain it. 
	So we dont need the return element which contain the property.'''
	variable = equation[0]
	equalTo = equation[1]
	if variable.conn == None :
		#means that variable is a selfvariable
		prop = matchProperties(element,variable.name)
	else:
		if element.connection[variable.conn].link != None:
			prop = matchProperties(element.connection[variable.conn].link.elem,variable.name)
		else :
			prop = None

	if prop != None:
		#clear variable.whoHelp before populating it with equalTo.evaluation()
		variable.clearwhoHelp()
		a = equalTo.evaluation(element,variable)
		return [prop,a,variable.whoHelp]
	else:
		pass


class Equation(object):
	'''Class that handle symbolic equation with the form expression = 0, and store every evidence possible of this equation in equationList
	Every variable contain in expression only once, will be highlight.
	If your variable appear twice, you need to add second expression where variable appear only once thise time'''
	def __init__(self):
		self.__equationList = [] #list of equations
		self.__todo = [] #list of expression = 0 that need to be converted in equation
	
	@property
	def equationList(self):
		return self.__equationList
	@property
	def todo(self):
		return self.__todo

	def addEquation(self,expression):
		#User add equation of kind expression = 0
		self.__todo.append((expression,K(0)))

	def addTodo(self,expression,equalTo):
		#Add equation to the todolist, used inside EquationElement class
		self.__todo.append((expression,equalTo))


	def processTodo(self):
		#Transform expression into equation with Variable in evidence.
		while len(self.todo) != 0 :
			tempTodo = list(self.todo)
			for x in tempTodo:
				x[0].evidence(x[1],self)
				self.__todo.remove(x)


class equationElement(object):
	def __init__(self):
		pass

	#Overloading operators
	def __add__(self,A):
		return Plus(self,A)

	def __sub__(self,A):
		return Minus(self,A)

	def __mul__(self,A):
		return Mul(self,A)

	def __div__(self,A):
		return Div(self,A)

	def __pow__(self,A):
		return Pow(self,A)

	def evidence(self,equalTo,equation):
		#Highlights variables in expressions
		return False

	def evaluation(self,element,variable):
		#Computing the value of the variable or constant which  call this method
		pass

class K(equationElement):
	''' Constant '''
	def __init__(self,value):
		#Value should be integer or float
		self.__value = value

	def evaluation(self,element,variable):
		return self.__value

class Var(equationElement):
	'''Class that represent variable. Variable got name,connection's kind,value and who help to compute this variable'''
	def __init__(self,name,connection):
		self.__name = name
		self.__conn = connection
		self.__value = None
		self.__whoHelp = set()

	@property
	def name(self):
		return self.__name
	@property
	def order(self):
		return self.__order
	@property
	def conn(self):
		return self.__conn
	@property
	def value(self):
		return self.__value
	@property
	def whoHelp(self):
		return self.__whoHelp

	def clearwhoHelp(self):
		self.__whoHelp = set()

	def evidence(self,equalTo,equa):
		equa.equationList.append((self,equalTo))

	def evaluation(self,element,variable):
		if element.connection[self.__conn].link != None:
			state = element.connection[self.__conn].link.elem
			result = matchProperties(state,self.__name)
			if (result != None and self.__name != 'fluid') or (type(result.value) != fluid.Fluid and self.__name == 'fluid'):
				variable.whoHelp.add(result)
				return result.value
			else :
				return None
		else:
			return None

class selfVar(Var):
	'''Same as variable, except that they dont have connection's kind. So they are linked to the element given in evaluation'''
	def __init__(self,name):
		Var.__init__(self,name,None)

	def evaluation(self,element,variable):
		result = matchProperties(element,self.name)
		if result != None:
			variable.whoHelp.add(result)
			return result.value
		else :
			return None



class RefVar(Var):
	'''Use RefVar if your equation is different than X - Xref = 0 EVEN with state equation. In case of state just put None as connection.'''
	def __init__(self,name,conn):
		Var.__init__(self,name,conn)

	def evaluation(self,element,variable):
		if self.conn != None:
			if element.connection[self.conn].link != None:
				state = element.connection[self.conn].link.elem
				result = matchProperties(state,self.name)
				if result != None:
					variable.whoHelp.add(result)
					return result.value
				else :
					return None
			else:
				return None
		else:
			result = matchProperties(element,self.name)
			if result != None:
				variable.whoHelp.add(result)
				return result.value
			else :
				return None

class selfRefVar(RefVar):
	'''Reference variable are particular, because they are linked with the reference state, wich is a boolean.
	So theyr evaluation is different from normal variable.
	Use this class ONLY for equation X - Xref = 0'''
	def __init__(self,name):
		RefVar.__init__(self,name,None)

	def evaluation(self,element,variable):
		result = matchProperties(element,self.name)
		refstate = element.properties['reference state']
		if result != None and refstate.value == True:
			variable.whoHelp.add(result)
			variable.whoHelp.add(refstate)
			return result.value
		else :
			return None

#############################
#Operator for usual equation#
#############################

class Plus(equationElement):
	#Overloading plus operator
	def __init__(self,A,B):
		self._left = A
		self._right = B

	def evidence(self,equalTo,equation):
		equation.addTodo(self._left,Minus(equalTo,self._right))
		equation.addTodo(self._right,Minus(equalTo,self._left))

	def evaluation(self,element,variable):
		A = self._left.evaluation(element,variable)
		B = self._right.evaluation(element,variable)
		if A == None or B == None :
			return None
		elif variable.name == 'quality' and (math.isnan(A) or math.isnan(B)):
			#equation that will acces to this part is : x = x_ph or x = x_ps
			return float('NaN')
		elif (math.isnan(A) or math.isnan(B)):
			#any other equation which wont compute quality will lies here if title = nan
			return None
		else:
			return A+B
		

class Minus(equationElement):
	#Overloading minus operator
	def __init__(self,A,B):
		self._left = A
		self._right = B

	def evidence(self,equalTo,equation):
		equation.addTodo(self._left,Plus(equalTo,self._right))
		equation.addTodo(self._right,Minus(self._left,equalTo))

	def evaluation(self,element,variable):
		A = self._left.evaluation(element,variable)
		B = self._right.evaluation(element,variable)
		if A == None or B == None :
			return None
		elif variable.name == 'quality' and (math.isnan(A) or math.isnan(B)):
			#equation that will acces to this part is : x = x_ph or x = x_ps
			return float('NaN')
		elif (math.isnan(A) or math.isnan(B)):
			#any other equation which wont compute quality will lies here if title = nan
			return None
		else:
			return A-B

class Mul(equationElement):
	#Overloading multiplication operator
	def __init__(self,A,B):
		self._left = A
		self._right = B

	def evidence(self,equalTo,equation):
		equation.addTodo(self._left,Div(equalTo,self._right))
		equation.addTodo(self._right,Div(equalTo,self._left))

	def evaluation(self,element,variable):
		A = self._left.evaluation(element,variable)
		B = self._right.evaluation(element,variable)
		if A == None or B == None or math.isnan(A) or math.isnan(B):
			return None
		else:
			return A*B


class Div(equationElement):
	#Overloading division operator
	def __init__(self,A,B):
		self._left = A
		self._right = B

	def evidence(self,equalTo,equation):
		equation.addTodo(self._left,Mul(equalTo,self._right))
		equation.addTodo(self._right,Div(self._left,equalTo))

	def evaluation(self,element,variable):
		A = self._left.evaluation(element,variable)
		B = self._right.evaluation(element,variable)
		if B == 0.0 and A != None :
			return None
		elif A == None or B == None or math.isnan(A) or math.isnan(B):
			return None
		else:
			return A/B

class Pow(equationElement):
	#Overloading power operator
	def __init__(self,A,B):
		self._base = A
		self._exponent = B

	def evidence(self,equalTo,equation):
		equation.addTodo(self._base,Pow(equalTo,Div(K(1),self._exponent)))
		equation.addTodo(self._exponent,Div(Log(equalTo,K(10)),Log(self._base,K(10)))) #Use logarithm with base 10

	def evaluation(self,element,variable):
		A = self._base.evaluation(element,variable)
		B = self._exponent.evaluation(element,variable)
		if A == None or B == None or math.isnan(A) or math.isnan(B):
			return None
		else:
			return math.pow(A,B)

class Log(equationElement):
	#Overloading logarithm operator
	def __init__(self,number,base):
		self._base = base
		self._number = number

	def evidence(self,equalTo,equation):
		equation.addTodo(self._number,pow(self._base,equalTo))
		equation.addTodo(self._base,Div(Ln(self._number),Ln(self._equalTo)))

	def evaluation(self,element,variable):
		resultNumber = self._number.evaluation(element,variable)
		resultBase = self._base.evaluation(element,variable)
		if resultNumber == None or resultBase == None or math.isnan(resultNumber) or math.isnan(resultBase):
			return None
		else :
			return math.log(resultNumber,resultBase)

class Ln(equationElement):
	#Ln is natural logarithm, and is needed to inverse Log operator
	def __init__(self,number):
		self._number = number

	def evaluation(self,element,variable):
		result = self._number.evaluation(element,variable)
		if result == None or math.isnan(result):
			return None
		else:
			return math.log(result)


####################
#Operator for fluid#
####################
class PropagateFluid(equationElement):
	'''Class that allow propagation of fluid between element'''
	def __init__(self,fluid1,fluid2):
		self._fluid1 = fluid1
		self._fluid2 = fluid2

	def evidence(self,equalTo,equation):
		equation.equationList.append((self._fluid1,self._fluid2))
		equation.equationList.append((self._fluid2,self._fluid1))

###################
#Operator for ref#
###################
class CheckNullExergy(equationElement):
	'''Check if reference state is True, in this case return 0, else return None'''
	def __init__(self,exergy):
		self._exergy = exergy

	def evidence(self,equalTo,equation):
		equation.equationList.append((self._exergy,CheckNullExergy(self._exergy)))

	def evaluation(self,element,variable):
		refstate = element.properties['reference state']
		if refstate.value == True :
			variable.whoHelp.add(refstate)
			return 0
		else :
			return None

class CheckRef(equationElement):
	'''Check if temperature, entropy and enthalpy equal reference value'''
	def __init__(self,refstate):
		self._refstate = refstate

	def evidence(self,equalTo,equation):
		equation.equationList.append((self._refstate,CheckRef(self._refstate)))

	def evaluation(self,element,variable):
		result = matchProperties(element,self._refstate.name)
		if result.isImposed == True and result.value == True :
			return True
		else :
			temp = element.properties['temperature']
			tempref = element.properties['reference temperature']
			entr = element.properties['entropy']
			entrref = element.properties['reference entropy']
			enth = element.properties['enthalpy']
			enthref = element.properties['reference enthalpy']
			var = matchProperties(element,variable.name)
			if (tempref.value == None or entrref.value == None or enthref.value == None or temp.value == None \
				or enth.value == None or entr.value == None) and var.value == None: #case if something is None and ref state is None
				return None
			elif (tempref.value == None or entrref.value == None or enthref.value == None or temp.value == None \
				or enth.value == None or entr.value == None) and var.value != None: #case if something is None and ref state is True/False then refstate need to be set at None. Because these module return only value that will be tell, its not possible to set None with a return. So we use variable.value = None
				var.value = None
				return None
			else: #case everythong is know, we check == and return what's needed.
				if temp.value == tempref.value and entr.value == entrref.value and enth.value == enthref.value:
					variable.whoHelp.add(temp)
					variable.whoHelp.add(tempref)
					variable.whoHelp.add(entr)
					variable.whoHelp.add(entrref)
					variable.whoHelp.add(enth)
					variable.whoHelp.add(enthref)
					return True
				else:
					return False

####################
#Operator for Flash#
####################
#Notes : fluid have by defaut fluid.Fluid as value. fluid.Fluid return None for every functions.
#Volume specific
''' On ne cherche normalement pas le volume specifique des fractions vapeurs ou liquides
class vv_p(equationElement):
	def __init__(self,pressure):
		self._pressure = pressure

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.vv_p(pressure)

class vl_p(equationElement):
	def __init__(self,pressure):
		self._pressure = pressure

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.vl_p(pressure)

class vv_T(equationElement):
	def __init__(self,temperature):
		self._temperature = temperature

	def evaluation(self,element,variable):
		temperature = self._temperature.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.vv_T(temperature)

class vl_T(equationElement):
	def __init__(self,temperature):
		self._temperature = temperature

	def evaluation(self,element,variable):
		temperature = self._temperature.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.vl_T(temperature)
'''

class v_pT(equationElement):
	def __init__(self,pressure,temperature):
		self._pressure = pressure
		self._temperature = temperature

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		qualityprop = element.properties['quality']
		quality = qualityprop.value
		if quality >= 0 and quality <= 1:
			variable.whoHelp.add(qualityprop)
			vv = fluid.vv_p(pressure)
			vl = fluid.vl_p(pressure)
			if vv != None and vl != None :
				return vv*quality + (1-quality)*vl
			else:
				return None
		elif quality != None:
			temperature = self._temperature.evaluation(element,variable)
			variable.whoHelp.add(qualityprop)
			return fluid.v_pT(pressure,temperature)
		else:
			return None

class v_ph(equationElement):
	def __init__(self,pressure,enthalpy):
		self._pressure = pressure
		self._enthalpy = enthalpy

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		qualityprop = element.properties['quality']
		quality = qualityprop.value
		if quality >= 0 and quality <= 1:
			variable.whoHelp.add(qualityprop)
			vv = fluid.vv_p(pressure)
			vl = fluid.vl_p(pressure)
			if vv != None and vl != None :
				return vv*quality + (1-quality)*vl
			else:
				return None
		elif quality != None:
			enthalpy = self._enthalpy.evaluation(element,variable)
			variable.whoHelp.add(qualityprop)
			return fluid.v_ph(pressure,enthalpy)
		else:
			return None

class v_ps(equationElement):
	def __init__(self,pressure,entropy):
		self._pressure = pressure
		self._entropy = entropy

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		qualityprop = element.properties['quality']
		quality = qualityprop.value
		if quality >= 0 and quality <= 1:
			variable.whoHelp.add(qualityprop)
			vv = fluid.vv_p(pressure)
			vl = fluid.vl_p(pressure)
			if vv != None and vl != None :
				return vv*quality + (1-quality)*vl
			else:
				return None
		elif quality != None:
			entropy = self._entropy.evaluation(element,variable)
			variable.whoHelp.add(qualityprop)
			return fluid.v_ps(pressure,entropy)
		else:
			return None

#Temperature
class Tsat_p(equationElement):
	'''Must not be used in State equation.'''
	def __init__(self,pressure):
		self._pressure = pressure

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.Tsat_p(pressure)

class T_px(equationElement):
	def __init__(self,pressure,quality):
		self._pressure = pressure
		self._quality = quality

	def evaluation(self,element,variable):
		quality = self._quality.evaluation(element,variable)
		pressure = self._pressure.evaluation(element,variable)
		if quality >=0 and quality <=1 :
			fluidprop = element.properties['fluid']
			fluid = fluidprop.value
			variable.whoHelp.add(fluidprop)
			return fluid.Tsat_p(pressure)
		else:
			return None

class T_ph(equationElement):
	def __init__(self,pressure,enthalpy):
		self._pressure = pressure
		self._enthalpy = enthalpy

	def evaluation(self,element,variable):
		enthalpy = self._enthalpy.evaluation(element,variable)
		pressure = self._pressure.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.T_ph(pressure,enthalpy)

class T_hs(equationElement):
	def __init__(self,enthalpy,entropy):
		self._entropy = entropy
		self._enthalpy = enthalpy

	def evaluation(self,element,variable):
		enthalpy = self._enthalpy.evaluation(element,variable)
		entropy = self._entropy.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.T_hs(enthalpy,entropy)

#Pressure
class psat_T(equationElement):
	'''Must not be used in state equation.'''
	def __init__(self,temperature):
		self._temperature = temperature

	def evaluation(self,element,variable):
		temperature = self._temperature.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.psat_T(temperature)

class p_Tx(equationElement):
	def __init__(self,temperature,quality):
		self._temperature = temperature
		self._quality = quality

	def evaluation(self,element,variable):
		quality = self._quality.evaluation(element,variable)
		temperature = self._temperature.evaluation(element,variable)
		if quality >=0 and quality <=1 :
			fluidprop = element.properties['fluid']
			fluid = fluidprop.value
			variable.whoHelp.add(fluidprop)
			return fluid.psat_T(temperature)
		else:
			return None

class p_hs(equationElement):
	def __init__(self,enthalpy,entropy):
		self._entropy = entropy
		self._enthalpy = enthalpy

	def evidence(self,equalTo,equation):
		#The first equation is useless, because we do not check the table. In addition, first equation return error because p_hs is not a variable. This is just to show the bearing of the 3 variable phs
		#equation.equationList.append( (p_hs(self._enthalpy,self._entropy),equalTo))
		#equation.equationList.append( (self._enthalpy,h_ps(equalTo,self._entropy)))
		#equation.equationList.append( (self._entropy,s_ph(equalTo,self._enthalpy)))
		pass

	def evaluation(self,element,variable):
		enthalpy = self._enthalpy.evaluation(element,variable)
		entropy = self._entropy.evaluation(element,variable)
		if element.genericName() == 'state':
			fluidprop = element.properties['fluid']
			fluid = fluidprop.value
			variable.whoHelp.add(fluidprop)
			return fluid.p_hs(enthalpy,entropy)
		else :
			#Fluid that will help to compute h_ps must be at least the fluid comming from the first conn.
			#Pay attention if you're dealing with heat exchanger
			if element.connection['I1'].link != None:
				state = element.connection['I1'].link.elem
				fluidprop = state.properties['fluid']
				fluid = fluidprop.value
				variable.whoHelp.add(fluidprop)
				return fluid.p_hs(enthalpy,entropy)
			else :
				return None

#Enthalpy
class hV_p(equationElement):
	def __init__(self,pressure):
		self._pressure = pressure

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.hV_p(pressure)

class hL_p(equationElement):
	def __init__(self,pressure):
		self._pressure = pressure

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.hL_p(pressure)

class hV_T(equationElement):
	def __init__(self,pressure):
		self._temperature = temperature

	def evaluation(self,element,variable):
		temperature = self._temperature.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.hV_T(temperature)

class hL_T(equationElement):
	def __init__(self,pressure):
		self._temperature = temperature

	def evaluation(self,element,variable):
		temperature = self._temperature.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.hV_T(temperature)

class h_pT(equationElement):
	def __init__(self,pressure,temperature):
		self._pressure = pressure
		self._temperature = temperature

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		temperature = self._temperature.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		#We need to check quality, to know what formula to use
		if variable.name[:9] != 'reference':
			qualityprop = element.properties['quality']
		elif variable.name[:9] == 'reference':
			qualityprop = element.properties['reference quality']
		quality = qualityprop.value
		if quality >= 0 and quality <= 1:
			HL = fluid.hL_T(temperature)
			HV = fluid.hV_T(temperature)
			variable.whoHelp.add(qualityprop)
			if HL != None and HV != None and (not math.isnan(HL)) and (not math.isnan(HV)):
				return HL*(1-quality)+HV*quality
			else :
				return None

		elif quality == None:
			if temperature >= fluid.Tcrit() or pressure >= fluid.pcrit(): 	#Case : fluid supercritic
				return fluid.h_pT(pressure,temperature)
			elif temperature != fluid.Tsat_p(pressure) :			#Case : fluid subcooled or superheated
				return fluid.h_pT(pressure,temperature)
			else :								#Case : fluid is diphasic
				return None

		elif math.isnan(quality):
			variable.whoHelp.add(qualityprop)
			return fluid.h_pT(pressure,temperature)



class h_ps(equationElement):
	def __init__(self,pressure,entropy):
		self._pressure = pressure
		self._entropy = entropy

	def evidence(self,equalTo,equation):
		#The first equation is useless, because we do not check the table. In addition, first equation return error because p_hs is not a variable. This is just to show the bearing of the 3 variable phs
		#equation.equationList.append((h_ps(self._pressure,self._entropy),equalTo))
		#equation.equationList.append((self._pressure,p_hs(equalTo,self._entropy)))
		#equation.equationList.append((self._entropy,s_ph(self._pressure,equalTo)))
		pass

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		entropy = self._entropy.evaluation(element,variable)
		if element.genericName() == 'state':
			fluidprop = element.properties['fluid']
			fluid = fluidprop.value
			variable.whoHelp.add(fluidprop)
			return fluid.h_ps(pressure,entropy)
		else :
			#Fluid that will help to compute h_ps must be at least the fluid comming from the first conn.
			#Pay attention if you're dealing with heat exchanger
			if element.connection['I1'].link != None:
				state = element.connection['I1'].link.elem
				fluidprop = state.properties['fluid']
				fluid = fluidprop.value
				variable.whoHelp.add(fluidprop)
				return fluid.h_ps(pressure,entropy)
			else:
				return None

class h_px(equationElement):
	def __init__(self,pressure,quality):
		self._pressure = pressure
		self._quality = quality

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		quality = self._quality.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.h_px(pressure,quality)

class h_Tx(equationElement):
	def __init__(self,temperature,quality):
		self._temperature = temperature
		self._quality = quality

	def evaluation(self,element,variable):
		temperature = self._temperature.evaluation(element,variable)
		quality = self._quality.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.h_Tx(temperature,quality)


#Entropy
class sV_p(equationElement):
	def __init__(self,pressure):
		self._pressure = pressure

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.sV_p(pressure)

class sL_p(equationElement):
	def __init__(self,pressure):
		self._pressure = pressure

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.sL_p(pressure)

class sV_T(equationElement):
	def __init__(self,pressure):
		self._temperature = temperature

	def evaluation(self,element,variable):
		temperature = self._temperature.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.sV_T(temperature)

class sL_T(equationElement):
	def __init__(self,pressure):
		self._temperature = temperature

	def evaluation(self,element,variable):
		temperature = self._temperature.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.sV_T(temperature)

class s_pT(equationElement):
	def __init__(self,pressure,temperature):
		self._pressure = pressure
		self._temperature = temperature

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		temperature = self._temperature.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		if variable.name[:9] != 'reference':
			qualityprop = element.properties['quality']
		elif variable.name[:9] == 'reference':
			qualityprop = element.properties['reference quality']
		quality = qualityprop.value
		if quality >= 0 and quality <= 1:
			SL = fluid.sL_T(temperature)
			SV = fluid.sV_T(temperature)
			if SL != None and SV != None and (not math.isnan(SL)) and (not math.isnan(SV)):
				variable.whoHelp.add(qualityprop)
				return SL*(1-quality)+SV*quality
			else:
				return None
		elif quality == None:
			if temperature >= fluid.Tcrit() or pressure >= fluid.pcrit(): 	#Case : fluid supercritic
				return fluid.s_pT(pressure,temperature)
			elif temperature != fluid.Tsat_p(pressure) :			#Case : fluid subcooled or superheated
				return fluid.s_pT(pressure,temperature)
			else :								#Case : fluid is diphasic
				return None


		elif math.isnan(quality):
			if temperature >= fluid.Tcrit() or pressure >= fluid.pcrit():
				variable.whoHelp.add(qualityprop)
				return fluid.s_pT(pressure,temperature)
			else :
				return None



class s_ph(equationElement):
	def __init__(self,pressure,enthalpy):
		self._pressure = pressure
		self._enthalpy = enthalpy

	def evidence(self,equalTo,equation):
		#The first equation is useless, because we do not check the table. In addition, first equation return error because p_hs is not a variable. This is just to show the bearing of the 3 variable phs.
		#equation.equationList.append((s_ph(self._pressure,self._enthalpy),equalTo))
		#equation.equationList.append((self._enthalpy,h_ps(self._pressure,equalTo)))
		#equation.equationList.append((self._pressure,p_hs(self._enthalpy,equalTo)))
		pass

	def evaluation(self,element,variable):
		enthalpy = self._enthalpy.evaluation(element,variable)
		pressure = self._pressure.evaluation(element,variable)
		if element.genericName() == 'state':
			fluidprop = element.properties['fluid']
			fluid = fluidprop.value
			variable.whoHelp.add(fluidprop)
			return fluid.s_ph(pressure,enthalpy)
		else :
			#Fluid that will help to compute h_ps must be at least the fluid comming from the first conn.
			#Pay attention if you're dealing with heat exchanger
			if element.connection['I1'].link != None:
				state = element.connection['I1'].link.elem
				fluidprop = state.properties['fluid']
				fluid = fluidprop.value
				variable.whoHelp.add(fluidprop)
				return fluid.s_ph(pressure,enthalpy)
			else:
				return None

#quality
class x_ph(equationElement):
	def __init__(self,pressure,enthalpy):
		self._pressure = pressure
		self._enthalpy = enthalpy

	def evaluation(self,element,variable):
		enthalpy = self._enthalpy.evaluation(element,variable)
		pressure = self._pressure.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.x_ph(pressure,enthalpy)

class x_ps(equationElement):
	def __init__(self,pressure,entropy):
		self._pressure = pressure
		self._entropy = entropy

	def evaluation(self,element,variable):
		pressure = self._pressure.evaluation(element,variable)
		entropy = self._entropy.evaluation(element,variable)
		fluidprop = element.properties['fluid']
		fluid = fluidprop.value
		variable.whoHelp.add(fluidprop)
		return fluid.x_ps(pressure,entropy)
