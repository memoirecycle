import wx
import logging
import element
logger = logging.getLogger()

matchKind=set([('toElem','fromElem')])

def match(kindA, kindB):
	''' Check if Connection's kind are matching '''
	if ((kindA, kindB) in matchKind) or ((kindB, kindA) in matchKind):
		return True
	else:
		logger.warning('Problem with match in Connection')
		return False



class Connection(object):
	def __init__(self, elem, kind,dpos):
		''' A Connection connect an Element and another Connection.
		A Connection always contains its own Element (elem).
		Its link contains a Connection (or is None) '''
		self.__dpos = dpos
		self.__elem = elem
		self.__kind = kind # see kind in matchKind
		self.__link = None
		self.__bmp = wx.Image("images/"+self.__kind+'.png').ConvertToBitmap()

	@property
	def bmp(self):
		''' Give the picture you see on panel. When a State is connected, its Connections are not visible. '''
		if type(self.__elem) == element.State and self.__link != None:
			self.__bmp.SetSize((0,0))
		else:
			self.__bmp.SetSize((12,12))
		return self.__bmp

	@property
	def dpos(self):
		''' Connection's pictures relative position'''
		if type(self.__elem) == element.State and self.__link != None:
			# drawn line in panel from connection start at the centre of state if connection.link != None
			return ( (self.__elem.zone[1][0]-self.__elem.zone[0][0])/2,(self.__elem.zone[1][1]-self.__elem.zone[0][1])/2)
		else:
			return self.__dpos 

	@property
	def elem(self):
		return self.__elem

	@property
	def kind(self):
		return self.__kind

	@property
	def link(self):
		return self.__link

	@link.setter
	def link(self,conn):
		""" link setter is used when trying to connect or deconnect two Connection """
		if conn == None: # = deconnection
			if self.__link == None:
				pass
			else :
				oldLink = self.__link
				self.__link.__link = None
				self.__link = None
				self.elem.connectionChanged()
				oldLink.elem.connectionChanged()
		elif type(self.elem)== element.State and type(conn.elem) == element.State:
			logger.info('Do not connect two states together')
		elif type(self.elem) == element.State or type(conn.elem) == element.State:
			if match(conn.__kind, self.__kind): # = connection to another conn OR change connection
				oldLink = self.__link
				oldConnLink = conn.__link
				if oldConnLink != None :
					# detach the previously connected
					conn.__link.__link = None
					oldLink.__link = None 
				self.__link = conn # connect to the new one
				conn.__link = self
				#The 4 connections involved tell they have changed
				self.elem.connectionChanged() 
				conn.elem.connectionChanged()
				if oldLink != None:
					oldLink.elem.connectionChanged()
				if oldConnLink != None:
					oldConnLink.elem.connectionChanged()
			else:
				logger.warning('Problem with kind in link.setter')
		else:

			logger.info('Please only connect state with transformation')
