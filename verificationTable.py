import XSteam #unit XSteam
import refprop #unit refprop
import fluid #unit SI



#SI
x = 4e6
y = 793.15

#------------------------------------------------------------------------------

fluid = fluid.FluidREFPROP(None,'WATER')
fF = fluid.h_pT(x,y)
'''fF = fluid.h_ps(x,y)
fF = fluid.h_px(x,y)
fF = fluid.h_Tx(x,y)
fF = fluid.s_pT(x,y)
fF = fluid.s_ph(x,y)
fF = fluid.x_ph(x,y)
fF = fluid.x_ps(x,y)'''
resultF = fF
print fF

#------------------------------------------------------------------------------
wm  = refprop.function('wm',0,0,'WATER')
b = wm/1000.0
'''
f = refprop.function('tsat_p',100,0,'WATER') #GOOD
f = refprop.function('tsat_s',5.5*1e3*b,0,'WATER')#GOOD
f = refprop.function('t_ph',4000,45.912*1e3*b,'WATER') #GOOD T:[10:500]+273.15 K p:[1:40]bar
f = refprop.function('t_ps',100,8.45*1e3*b,'WATER')#GOOD
f = refprop.function('t_hs',457*1e3*b,1.5*1e3*b,'WATER')  #difference legere with xsteam, mais probablement dans des zones speciales
f = refprop.function('psat_t',373.15,0,'WATER') #GOOD
f = refprop.function('psat_s',4.8*1e3*b,0,'WATER')#Good meme si legere difference.
f = refprop.function('p_hs',3800*1e3*b,7.5*1e3*b,'WATER')#Ne fonctionne que dans certaine zone !
'''

f = refprop.function('h_pt',x/1e3,y,'WATER')/b
'''f = refprop.function('h_ps',x/1e3,y*b,'WATER')/b
f = refprop.function('h_px',x/1e3,y,'WATER')/b
f = refprop.function('h_Tx',x,y,'WATER')/b
f = refprop.function('s_pT',x/1e3,y,'WATER')/b
f = refprop.function('s_ph',x/1e3,y*b,'WATER')/b
f = refprop.function('x_ph',x/1e3,y*b,'WATER')
f = refprop.function('x_ps',x/1e3,y*b,'WATER')'''
result = f


#------------------------------------------------------------------------------
fX = XSteam.function('h_pT',x/1e5,y-273.15)
#fX = XSteam.function('h_ps',x/1e5,y/1e3)
#fX = XSteam.function('h_px',x/1e5,y)
#fX = XSteam.function('h_Tx',x-273.15,y)
#fX = XSteam.function('s_pT',x/1e5,y-273.15)
#fX = XSteam.function('s_ph',x/1e5,y/1e3)
#fX = XSteam.function('x_ph',x/1e5,y/1e3)
#fX = XSteam.function('x_ps',x/1e5,y/1e3)
resultX = fX




#print result
print 'refprop :',result, 'xsteam :',resultX,'fluid.py :',resultF


'''
h = j/mole

wm = g/mole
		b = kg/mole = wm/a
a = 1000 g/kg

j/kg => h/b = h*a/wm
'''
