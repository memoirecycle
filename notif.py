class Notifier(object):
	"""Give notification"""
	def __init__(self):
		self.__notifyList = set()

	def addNotif(self,x):
		self.__notifyList.add(x)

	def removeNotif(self,x):
		self.__notifyList.remove(x)

	def doNotify(self): 
		tempNotifyList = list(self.__notifyList)
		for x in tempNotifyList:
			x.OnNotif()
			

class Notifiable(object):
	"""Receive notification"""
	def __init__(self):
		pass

	def OnNotif(self):
		pass
