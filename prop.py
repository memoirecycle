import notif
import wx.propgrid
import logging
import fluid
import math
import equa

logger = logging.getLogger()

def qualityKind(prop):
	#Check if the fluid is subcooled,superheated or supercritic, and return it
	for x in prop.project.elemList:
		if x[:5] == 'state':
			for y in prop.project.elemList[x].properties:
				if prop == prop.project.elemList[x].properties[y]:
					elem = prop.project.elemList[x]
	fluid = elem.properties['fluid'].value
	temperature = elem.properties['temperature'].value
	pressure = elem.properties['pressure'].value
	tcrit = fluid.Tcrit()
	tsat = fluid.Tsat_p(pressure)
	if temperature < tcrit:
		if temperature < tsat :
			return 'subcooled'
		elif temperature > tsat :
			return 'superheated'
	else:
		return 'supercritic'



class Property(notif.Notifier):
	''' Property has a value and a label. Element contains Property in its properties dictionnary.'''
	def __init__(self,project,label,value = None):
		notif.Notifier.__init__(self)
		self._isImposed = False
		self._isTell = False
		self._label = label
		self._labelKey = None
		self._theyDependOnMe = set()
		self._value = value
		self._wantNotif = set() #Take element which want to be notify when these property change, and wich have an update() method
		self.__project = project

	@property
	def isImposed(self):
		return self._isImposed

	@isImposed.setter
	def isImposed(self, val):
		self._isImposed = val

	@property
	def isTell(self):
		return self._isTell
	@property
	def label(self):
		return self._label
	@property
	def labelKey(self):
		return self._labelKey

	@labelKey.setter
	def labelKey(self, val):
		self._labelKey = val

	@property
	def project(self):
		return self.__project
	@property
	def value(self):
		return self._value
	@value.setter
	def value(self,val):
		self._value = val
	@property
	def wantNotif(self):
		return self._wantNotif

	def relax(self):
		pass

	def colour(self):
		if self._isImposed == True:
			return 'BLUE'
		elif self._isTell == True:
			return 'GREEN'
		else:
			return 'BLACK'

class Variable(Property):
	''' Variable are Property that can be tell (only)'''
	def __init__(self,project,label,value=None):
		Property.__init__(self,project,label,value)
		self._iDependOnThem = set()

	def addDependence(self, on):
		self._iDependOnThem |= on
		for x in on :
			x._theyDependOnMe.add(self)

	def clearDependence(self):
		for x in self._iDependOnThem:
			assert self in x._theyDependOnMe
			x._theyDependOnMe.remove(self)#Remove from the dependance so nobody else depend on me
		self._iDependOnThem.clear() # Clear my dependance so I depend on nobody

	def relax(self):
		""" Empty the value and do notification"""
		''' You have to check if it's not impose '''
		if self._isImposed:
			return True
		self._value = None
		self._isTell = False
		theydependedOnMe=self._theyDependOnMe.copy()
		self.clearDependence() # first
		for x in theydependedOnMe: # second : order important
			x.relax()
		assert self._iDependOnThem == set()
		assert self._theyDependOnMe == set()
		self.project.todoList  |= self.wantNotif

		
	def impose(self, val):
		''' Impose a value and do notification '''
		assert val!=None
		assert self._isTell == False
		self._isImposed = False
		self.relax()
		if val == '': # relax
			self._isImposed = False
			self.relax() #force the value out
			logger.info('relax %s',self.label)
		elif val == u'NaN':
			self._isImposed = True
			self._value = float('NaN')
			logger.info('impose %s with a value of %s',self.label,self._value)
		else:
			self._isImposed = True
			self._value = val
			logger.info('impose %s with a value of %s',self.label,self._value)
		self.project.doNotify()

	def tell(self, val, becauseOf):
		''' Property can tell another property. It spreads the value troughout the cycle.
		Creates dependance and notification '''
		if val == None:
			return True
		if val == self._value:
			if not self._isImposed:
				self.addDependence(becauseOf)
				pass
			else:
				assert self._iDependOnThem == set()
			return True			
		elif self._value == None :
			assert self._iDependOnThem == set()
			assert self._theyDependOnMe == set()
			self.relax() #Just for notifications
			self._value=val
			self.addDependence(becauseOf)
			self._isTell = True
			if type(self) != FluidProperty:
				logger.info('tell %s with a value of %s',self.label,val)
			else:
				logger.info('tell %s with a value of %s',self.label,val.name)
			return True
		else:
			logger.info('tell %s with a value of %s \n but already have %s',self.label,val,self._value)
			return False # Property was told or impose and someone try to impose another value

class FloatVariableProperty(Variable):
	''' Variable who's value is a float'''
	def __init__(self,project,label,value=None):
		Variable.__init__(self,project,label,value)
		self._display = None 

	@property
	def value(self):
		if self._value != None:
			return float(self._value)
		else:
			return None
	@property
	def display(self):
		if self._value == None :
			self._display = wx.propgrid.StringProperty(self.label)
			return self._display
		else:
			self._display = wx.propgrid.StringProperty(self.label,value = str('%0.2f'%float(self._value)))
			return self._display
			
class StringVariableProperty(Variable):
	''' Variable who's value is a string'''
	def __init__(self,project,label,value=None):
		Variable.__init__(self,project,label,value)
		self._display = None

	@property
	def display(self):
		if self._value == None :
			self._display = wx.propgrid.StringProperty(self.label)
			return self._display
		else:
			self._display = wx.propgrid.StringProperty(self.label,value = str(self._value))
			return self._display


class FixedProperty(Property):
	''' Properties that can not be tell by the programm '''
	def __init__(self,project,label):
		Property.__init__(self,project,label)

	def impose(self, val):
		self.isImposed = True
		self.value = val
		self.project.doNotify()
		

class FloatFixedProperty(FixedProperty):
	''' Float properties that can not be tell '''
	def __init__(self,project,label):
		FixedProperty.__init__(self,project,label)
		self._display = None
	@property
	def value(self):
		if self._value != None:
			return float(self._value)
		else:
			return None
	@property
	def display(self):
		if self._value == None :
			self._display = wx.propgrid.StringProperty(self.label)
			return self._display
		else:
			self._display = wx.propgrid.StringProperty(self.label,value = str(self._value))
			return self._display


class StringFixedProperty(FixedProperty):
	''' String properties that can not be tell'''
	def __init__(self,project,label):
		FixedProperty.__init__(self,project,label)
		self._display = None
	@property
	def value(self):
		if self._value != None:
			return str(self._value)
		else:
			return None
	@value.setter
	def value(self,val):
		self._value = val

	@property
	def display(self):
		if self._value == None :
			self._display = wx.propgrid.StringProperty(self.label)
			return self._display
		else:
			self._display = wx.propgrid.StringProperty(self.label,value = str(self._value))
			return self._display


class TellProperty(Variable): 
	''' All the properties that only can be tell but can't be imposed'''
	def __init__(self,project,label,value = None):
		Variable.__init__(self,project,label,value)

	def impose(self, val):
		return False

class FloatTellProperty(TellProperty):
	''' Float properties that can be tell '''
	def __init__(self,project,label,value=None):
		TellProperty.__init__(self,project,label,value)
		self._display = None

	@property
	def display(self):
		if self._value == None :
			self._display = wx.propgrid.StringProperty(self.label)
			return self._display
		else:
			self._display = wx.propgrid.StringProperty(self.label,value = str('%0.5f'%float(self._value)))
			return self._display


class BoolProperty(Variable):
	''' Boolean properties '''
	def __init__(self,project,label,value=None):
		Variable.__init__(self,project,label,value)
		self._value = None
		self._display = None
	@property
	def value(self):
		return self._value
	@value.setter
	def value(self,val):
		self._value = val

	@property
	def display(self):
		if self._value != None :
			return wx.propgrid.BoolProperty(self.label,value = self.value)
		else:
			return wx.propgrid.BoolProperty(self.label)


class FluidProperty(Variable):
	''' Fluid properties '''
	def __init__(self,project,label,value=None):
		Variable.__init__(self,project,label,value=fluid.Fluid(project,''))
		self._display = None
	@property
	def value(self):
		return self._value

	@property
	def display(self):
		arrayId = range(len(self.project.app.fluidList))
		fluidname = self.value.name
		num = self.project.app.fluidListName.index(fluidname)
		return wx.propgrid.EnumProperty('fluid','fluid',self.project.app.fluidListName,arrayId,num)

	def relax(self):
		if self._isImposed:
			return True
		self._value = self.project.app.fluidList['']
		self._isTell = False
		theydependedOnMe=self._theyDependOnMe.copy()
		self.clearDependence() # first
		for x in theydependedOnMe: # second : order important
			x.relax()
		assert self._iDependOnThem == set()
		assert self._theyDependOnMe == set()
		self.project.todoList  |= self.wantNotif


	def impose(self, val):
		assert val!=None
		assert self._isTell == False
		self.relax()
		if type(val) == fluid.Fluid: # relax
			self._value = val
			self._isImposed = False
			self.relax()
			logger.info('relax %s',self.label)
		else:
			self._isImposed = False
			self.relax()
			self._isImposed = True
			self._value = val
			logger.info('impose %s with a value of %s',self.label,self._value.name)
		self.project.doNotify()

	def tell(self, val, becauseOf):
		if val == None :
			return True
		if val == self._value:
			if not self._isImposed:
				self.addDependence(becauseOf)
				pass
			else:
				assert self._iDependOnThem == set()
			return True			
		elif type(self._value) == fluid.Fluid:
			assert self._iDependOnThem == set()
			assert self._theyDependOnMe == set()
			self.relax() #Just for notifications
			self._value=val
			self.addDependence(becauseOf)
			self._isTell = True
			logger.info('tell %s with a value of %s',self.label,val.name)
			return True
		else:
			logger.info('tell %s with a value of %s \n but already have %s',self.label,val.name,self._value.name)
			return False # Property was told or impose and someone try to impose another value



class QualityProperty(Variable):
	''' Quality properties '''
	def __init__(self,project,label,value = None):
		Variable.__init__(self,project,label,value)
		self._display = None 
	@property 
	def value(self):
		if self._value != None:
			return float(self._value)
		else:
			return None

	@property
	def display(self):
		if self._value == None :
			self._display = wx.propgrid.StringProperty(self.label)
		elif not math.isnan(float(self._value)):
			self._display = wx.propgrid.StringProperty(self.label,value = str(self._value))
		elif math.isnan(float(self._value)):
			labelVal = qualityKind(self)
			self._display = wx.propgrid.StringProperty(self.label,value = labelVal)
		return self._display	


	def tell(self, val, becauseOf):
		''' Property can tell another property. It spreads the value troughout the cycle.
		Creates dependance and notification '''
		if val == None:
			return True
		if val == self._value:
			if not self._isImposed:
				self.addDependence(becauseOf)
				pass
			else:
				assert self._iDependOnThem == set()
			return True
		elif self._value == None :
			assert self._iDependOnThem == set()
			assert self._theyDependOnMe == set()
			self.relax() #Just for notifications
			self._value=val
			self.addDependence(becauseOf)
			self._isTell = True
			logger.info('tell %s with a value of %s',self.label,val)
			return True
		elif val != None and self._value != None:
			if math.isnan(float(val)) and math.isnan(float(self._value)):
				if not self._isImposed:
					self.addDependence(becauseOf)
					pass
				else:
					assert self._iDependOnThem == set()
				return True
		else:
			logger.info('tell %s with a value of %s \n but already have %s',self.label,val,self._value)
			return False # Property was told or impose and someone try to impose another value
